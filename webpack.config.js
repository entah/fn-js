const path = require('path');

module.exports = {
  target: 'web',
  mode: 'production',
  entry: './index.js',
  output: {
    filename: 'fn.js',
    path: path.resolve(__dirname, 'dist'),
  },
};
