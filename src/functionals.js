'use strict';

/**
 * return the arg supplied. this maybe tho no-op function that return it's arg
 *
 * @example 
 * `identity(3); //=> 3
 identity(); //=> undefined`
 * 
 * @param {any} arg
 * @return {any} the arg supplied
 */

const identity = arg => {
  return arg;
};



/**
 * return function on evaluation will constantly return the same arg.
 *
 * @example
 * `var cnts = constantly(3); cnts(10); //=> 3`
 * 
 * @param {any} arg
 * @return {function} the function that on evaluation constantly return the same arg
 */

const constantly = arg => {
  const argToReturn = clone(arg);
  return () => argToReturn;
};


/**
 * check if arg is function type
 *
 * @param {*} maybeFn - arg to check
 * @return {boolean} true if arg is function type
 */

const isFn = maybeFn => {
  return (typeof maybeFn === 'function');
};



/**
 * check if arg is Array type
 *
 * @param {*} maybeArray - arg to check
 * @return {boolean} true if arg is Array type
 */

const isArr = maybeArray => {
  return Array.isArray(maybeArray);
};


/**
 * check if arg is Object type. if isStrict is true will make sure the arg passed constructor test
 *
 * @param {*} maybeObject - arg to check
 * @param {Boolean}[isStrict] - make sure the arg passed constructor test
 * @return {boolean} true if arg is Object type
 */

const isObj = (maybeObject, isStrict) => {
  if (isStrict){
    return maybeObject ? (!isArr(maybeObject) && (typeof maybeObject === 'object') && maybeObject !== null && (maybeObject.constructor === Object)) : false;
  }
  return maybeObject ? (!isArr(maybeObject) && (typeof maybeObject === 'object') && maybeObject !== null) : false;
};


/**
 * check if arg is String type
 *
 * @param {*} maybeString - arg to check
 * @return {boolean} true if arg is String type
 */

const isStr = maybeString => {
  return (typeof maybeString === 'string');
};


/**
 * check if arg is `Generator` object.
 *
 * @param {any} arg - arg to check
 * @return {Boolean} true if arg is `Generator` object
 */

const isGen = arg => {
  if (!arg) return false;
  return (!isStr(arg)) && (Symbol.iterator in Object(arg) && !isArr(arg));
};


/**
 * check if arg is Number Type
 *
 * @param {any} arg -arg to check
 * @return {Boolean} true if ars is Number type.
 */

const isNum = arg => {
  return (typeof arg === 'number') && !isNaN(arg);
};


/**
 * check if arg is odd number
 *
 * @param {Number} arg - arg to check
 * @param {Boolean} - true if arg is odd number
 */

const isOdd = arg => {
  if (isNum(arg)) {
    return (arg % 2) === 1;
  }
  return false;
};


/**
 * check if arg is even number
 *
 * @param {Number} arg - arg to check
 * @param {Boolean} - true if arg is even number
 */

const isEven = arg => {
  if (isNum(arg)) {
    return (arg % 2) === 0;
  }
  return false;
};


/**
 * check if collection is empty
 *
 * @param {(Object|Array|string)} coll - collection to count.
 * @return {boolean} true if collection is empty
 * @throws if arg is non-collection.
 */

const isEmpty = coll => {
  return count(coll) < 1;
};


/**
 * check if arg is collection
 *
 * @example
 * `isColl({a: 1}); //=> true 
 isColl([1, 2]); //=> true 
 isColl("hello"); //=> true`
 * 
 * @param {any} maybeCollection - arg to check
 * @return {boolean} true if arg is collection, false otherwise
 */

const isColl = maybeCollection => {
  return isArr(maybeCollection) || isObj(maybeCollection, true) || isStr(maybeCollection);
};


/**
 * count the elements on an item.
 *
 * @param {(Object|Array|string)} coll - collection to count.
 * @return {number} the number of elements on collection.
 * @throws if arg is non-collection.
 */


const count = coll => {
  if (typeof coll === 'number') throw new Error("Argument cannot be a number type");
  if (isObj(coll, true)){
    return Object.keys(coll).length;
  }
  return coll.length;
};


/**
 * return the first element of a collection
 *
 * @example
 * `first([1, 3, 4]); //=> 1
 first({a: 1, b: 2}); //=> ["a" 1] 
 first("hello"); //=> "h"`
 *
 * @param {(Object|Array|string)} coll - collection
 * @return {*} the first element of a collection, undefined when empty collection supplied, and null on non-collection type arg.
 *
 */

const first = coll => {
  if (isObj(coll, true)) {
    return first(Object.entries(coll));
  }

  if (isStr(coll)){
    return coll.charAt(0);
  }

  if (isArr(coll)) {
    return [...coll].shift();
  }

  return null;
};


/**
 * return the last element of a collection
 *
 * @example
 * `last([1, 3, 4]); //=> 4 
 last({a: 1, b: 2}); //=> ["b" 2] 
 last("hello"); //=> "o"`
 *
 * @param {(Object|Array|string)} coll - collection
 * @return {*} the last element of a collection, undefined when empty collection supplied, and null on non-collection type arg.
 *
 */

const last = coll => {
  if (isObj(coll, true)) {
    return last(Object.entries(coll));
  }

  if (isStr(coll)){
    return coll.charAt(coll.length - 1);
  }

  if (isArr(coll)){
    return [...coll].pop();
  }

  return null;
};


/**
 * return a new array of elements from collection without the last element.
 *
 * @example
 * `butLast([1, 3, 4]); //=> [1, 3]
 butLast({a: 1, b: 2}); //=> [["a" 1]] 
 butLast("hello"); //=> ["h", "e", "l", "l"]`
 *
 * @param {(Object|Array|string)} coll - collection
 * @return {(Array | null)} array of elements from collection without the last element or null on non-collection type arg.
 *
 */

const butLast = coll => {
  if (isObj(coll, true)) {
    return butLast(Object.entries(coll));
  }

  if (isStr(coll)){
    if (coll.length > 1){
      return coll.substring(0, coll.length - 1);
    }

    return coll;
  }

  if (isArr(coll)){
    if (coll.length > 1){
      const newColl = [...coll];
      newColl.pop();
      return newColl;
    }

    return coll;
  }

  return null;
};


/**
 * return a new array of elements from collection without the first element.
 *
 * @example
 * `rest([1, 3, 4]); //=> [3, 4] 
 rest({a: 1, b: 2}); //=> [["b" 2]]
 rest("hello"); //=> ["e", "l", "l", "o"]`
 *
 * @param {(Object|Array|string)} coll - collection
 * @return {(Array | null)} array of elements from collection without the first element or null on non-collection type arg.
 *
 */

const rest = coll => {
  if (isObj(coll, true)) {
    return rest(Object.entries(coll));
  }

  if (isStr(coll)){
    if (coll.length > 1) {
      return coll.substring(1, coll.length);
    }

    return coll;
  }

  if (isArr(coll)){
    if (coll.length > 1){
      const newColl = [...coll];
      newColl.shift();
      return newColl;
    }

    return coll;
  }

  return null;
};


/**
 * return a new array of each element in collection after evaluated by function fn arg. when no collection supllied, return transducer function.
 *
 * @param {function} fn - function to evaluate each element. receive same args as using Array.prototype.map method.
 * @param {(Object|Array|string)} coll - collection
 * @return {(Array | function)} return a new array of each element in collection after evaluated by function fn
 * @throws if arg is non-collection.
 */

const map = (fn, coll) => {
  if (coll) {
    if (isObj(coll, true)){
      return map(fn, Object.entries(coll));
    }

    if (isStr(coll)) {
      return map(fn, coll.split(''));
    }

    return coll.map(fn);
  }

  return partial(map, fn);
};


/**
 * return a result of each element in collection after evaluated by function fn arg. when no collection supllied, return transducer function.
 *
 * @param {function} fn - function to evaluate each element. receive same args as using Array.prototype.reduce method.
 * @param {(Object|Array|string)} coll - collection
 * @param {any} [init] init - optional init value
 * @return {*} return a new array of each element in collection after evaluated by function fn
 * @throws if arg is non-collection.
 */

const reduce = (fn, coll, init) => {
  if (!(coll)) return partial(reduce, fn);
  if (init) return iterable(coll).reduce(fn, init);
  return iterable(coll).reduce(fn);
};



/**
 * return iterable object or array of elements of collection, otherwise return null
 * 
 * @param {any} coll - arg to transform to iterable
 * @return {(array | null)} return array for collection like Object, Array, or String, otherwise null
 */

const iterable = coll => {
  if (isObj(coll, true)){
    return Object.entries(coll);
  }

  if (isStr(coll)) {
    return coll.split('');
  }

  if (isArr(coll)){
    return [...coll];
  }

  return null;
};


/**
 * create a new Object from an array consising key and value pairs like `[key, value]`.
 * reverse operation of `iterable` with object as argument.
 *
 * @example
 * `reObj([["a", 1], ["b", 2], ["c", 3]);
 //=> {a: 1, b: 2, c: 3}`
 *
 * @param {Array} arr - array consisting key and value pairs
 * @return {Object} new object
 */

const reObj = arr => {
  if (isArr(arr)){
    return reduce((init, [k, v]) => {
      return {...init, [k]: v};
    }, arr, {});
  }

  return null;
};


/**
 * return an object for every element on coll aggregate by fn. return null if coll not collection type.
 * be aware of deep structure reference.
 * 
 * @param {function} fn - function to aggregate
 * @param {any} coll - collection to grouping
 * @return {(array | null)} return object contained aggregated object, otherwise null
 */

const groupBy = (fn, coll) => {
  if (isObj(coll, true)){
    return groupBy(fn, Object.entries(coll));
  }

  if (isStr(coll)) {
    return groupBy(fn, coll.split(''));
  }

  if (isArr(coll)){
    return reduce((init, next) => {
      const key = fn(next);
      if (key in init){
	return {...init, ...{[key]: [...init[key], next]}};
      }

      return {...init, ...{[key]: [next]}};
    }, coll, {});
  }

  return null;
};



/**
 * return an object consisting element as key and frequency as value inside coll. return null if coll cannot be aggregated
 * 
 * @param {any} coll - collection that can be aggregated
 * @return {(array | null)} return frequencies object, otherwise null
 */

const freq = coll => {
  const aggregated = groupBy(identity, coll);
  if (aggregated) {
    return reduce((init, [key, val]) => {
      return {...init, ...{[key]: count(val)}};
    }, aggregated, {});
  }

  return null;
};



/**
 * return an object by iterating elements in 2 array at same time untll one of those array exhausted.
 * return null if both of arg not an array.
 * be aware of deep structure reference.
 * 
 * @param {array} keysColl - array of keys
 * @param {array} valsColl - array of vals
 * @return {(array | null)} return an object, otherwise null
 */

const zipMap = (keysColl, valsColl) => {
  if (isColl(keysColl) && isColl(valsColl)) {
    if (count(keysColl) > count(valsColl) ){
      return reduce((init, next, index) => {
	return {...init, ...{[keysColl[index]] : next}};
      }, valsColl, {});
    }

    return reduce((init, next, index) => {
      return {...init, ...{[next] : valsColl[index]}};
    }, keysColl, {});
  }

  return null;
};



/**
 * return a new object by take entries found within a keys. return null if obj isn't Object type
 * be aware of deep structure reference.
 *
 * @example
 * `selectKeys({a: 1, b: 2, c: 3}, ["a", "c"]);
 //=> {a: 1, c: 3}`
 *
 * @param {object} obj - object to take the entries
 * @param {array} keys - an array of keys to pulling out entries
 * @return {(array | null)} return an object, otherwise null
 */

const selectKeys = (obj, keys) => {
  if (isObj(obj)){
    if (isArr(keys)) {
      return reduce((init, next) => {
	if (next in obj) {
	  return {...init, ...{[next]: obj[next]}};
	}
	return init;
      }, keys, {});
    }

    return null;
  }

  return null;
};


/**
 * return object that entries dropped by associate keys
 * be aware of deep structure reference.
 *
 * @example
 * `dropKeys({a: 1, b: 2, c: 3}, ["a", "c"]);
 //=> {b: 2}`
 *
 * @param {Object} obj - object to take the entries
 * @param {Array} keys - an array of keys to pulling out entries
 * @return {(Array | Null)} return an object, otherwise null
 */

const dropKeys = (obj, keys) => {
  if (isObj(obj)) {
    if (isArr(keys)){
      return reduce((init, [k, v]) => {
	return keys.indexOf(k) < 0 ? {...init, [k]: v} : init;
      }, obj, {});
    }

    return null;
  }

  return null;
};


/**
 * return a result of evaluated flattened  args and applying it to the function fn arg. the last args must a collection type.
 *
 * @example
 * `apply( console.log, "hello ", "joko", ["jono", "nina", "bobo"]);
 //=> console.log ", "joko", "jono", "nina", "bobo")`
 *
 * @param {function} fn - function to receive flatted args
 * @param {...any} any - any arguments, the last argument must a coll type
 * @return {*} evaluated args
 * @throws if the last arg is non-collection type.
 */

const apply = (fn, ...args) => {
  if (args.length > 2) {
    return fn(...butLast(args), ...last(args));
  }

  if (args.length == 2) {
    return fn(first(args), ...last(args));
  }

  return fn(...first(args));
};


/**
 * return a function that would receive some of the args rather than a full args for the fn arg.
 *
 * @example
 * `var sum = (a, b); //=> a + b
 var partialSum = partial(sum, 1);
 partialSum(2); //=> 3
 partialSum(10); //=> 11`
 *
 * @param {function} fn - function to receive flatted args
 * @param {...any} args - some args to supply to the fn arg
 *
 * @return {function} function that would be receive the rest of args.
 * @throws if the last arg is non-collection type.
 */


const partial = (fn, ...args) => {
  return (...restArgs) => fn(...args, ...restArgs);
};


/**
 * return the result of evaluating the arg. if arg is not a function, it will evaluate using eval();
 *
 * @param {any} arg - arg that will be evaluate
 * @return {any} the result of evaluating the arg
 */

const evaluate = arg => {
  if (isFn(arg)){
    return arg();
  }

  return eval(arg);
};


/**
 * evaluate all args (lazy function) and return the result of the last arg
 * 
 * @param {...function} lazyFns - an args contain function to evaluate
 * @return {any} the result of evaluating the last arg
 */


const doAll = (...lazyFns) => {
  return lazyFns.reduce((_acc, lazyFn) => {
    return evaluate(lazyFn);
  }, null);
};



/**
 * return function that composed of lazy functions supplied as args. the result of each function 
 * will supplied to the next function. the execution flow is from left to right. this only work
 * for synchronous functions. for asynchronous see `_compose`
 * 
 * @param {...function} fns - sequence of functions to execute
 * @return {function} Synchronous functions composed of args
 */

const compose = (firstFn, ...fns) => {
  return (...params) => reduce((init, fn) => fn(init), fns, firstFn(...params));
  /* return function(...params) {
   *   const cl = function(fn, fns, ...args) {
   *     if (fns.length == 0) {
   return fn(...args);
   *     }

   *     if (fns.length == 1) {
   return first(fns)(fn(...args));
   *     }

   *     return cl(first(fns), rest(fns), fn(...args))
   *   }

   *   return cl(firstFn, fns,  ...params)
   * }; */
};



/**
 * return function that composed of lazy functions supplied as args. the result of each function 
 * will supplied to the next function. the execution flow is from left to right. this only work
 * for asynchronous functions. for synchronous see `compose`
 * 
 * @param {...function} fns - sequence of functions to execute
 * @return {function} Asynchronous functions composed of args
 */

const _compose = (firstFn, ...fns) => {
  return (...params) => {
    const initialValue = new Promise((res) => {
      res(firstFn(...params));
    });
    return reduce((promise, nextFn) => promise.then(res => nextFn(res)), fns, initialValue);
  };
};


/**
 * get a value from an object or an array
 *
 * @param {(Object | Array)} coll - value to get to
 * @param {any} key - key for an object or index for an array where the value is
 * @return {any} return the value from a coll by given key
 */

const get_ = (coll, key) => {
  if (isObj(coll) || isArr(coll)){
    return coll[key];
  }

  return null;
};


/**
 * get the value from a (maybe deep) structured collection
 * be aware of deep structure reference.
 * 
 * @example
 * `getIn({a: 1 , b: 2, c: 3}, ["a"]); //=> 1 
 getIn([1, {a: [2, [2, 3 ,4, [10]]]}], [1, "a", 2, 4, 1]); //=>  10`
 *
 * @param {(object | array)} coll - (maybe deep) structured object or array
 * @param {array} paths - array consising key for Object or index for Array
 * @return {any} the value returned from path in the collection
 *
 */

const getIn = (coll, paths) => {
  if ((isObj(coll) || isArr(coll)) && isArr(paths)){
    if (!isEmpty(coll) && !isEmpty(paths)) {
      return reduce((init, next) => {
	return get_(init,next);
      }, paths, coll);
    }

    return null;
  }

  return null;
};


/**
 * clone the arg recursively, if the arg isn't Object or Array type, return it's arg.
 * take a note, clone only work in Object if the constructor property is an object type.
 *
 * @param {any} arg - argument to clone
 * @return {any} cloned arg or the arg itself
 */

const clone = arg => {
  if (isObj(arg, true)) {
    return reduce((init, [key, val]) => {
      if (isObj(val, true) || isArr(val)) {
	return {...init, [key]: trampoline(clone, val)};
      }

      return {...init, [key]: val};
    }, arg, {});
  }

  if (isArr(arg)){
    return reduce((init, next) => {
      if (isObj(next, true) || isArr(next)) {
	return [...init, trampoline(clone, next)];
      }

      return [...init, next];
    }, arg, []);
  }

  return arg;
};


/**
 * accumutaled the return value from function or generator object. if the arg is function type execute it until the return value become undefined.
 * work with `range` and `repeatedly` function.
 * 
 * @param {(Function | Generator)} arg - the arg to accumulate the return value
 * @return {any} value accumulated
 */

const takeAll = arg => {
  const result = [];
  if (isFn(arg)) {
    let run = true;
    while (run) {
      const evaluated = arg();
      if (evaluated !== undefined) {
	result.push(evaluated);
	continue;
      }
      break;
    }
    return result;
  }

  if (isGen(arg)){
    for (const i of arg) {
      result.push(i);
    }
  }

  return result;
};



/**
 * return function when executed return sequence of the range. this function wrap `FunctionGenerator`.
 * start and stop is an optional and default to 0.
 * be carefull to assign this function to global variable, since the counter cannot be reset. 
 * use `take`, `drap`, and/or `takeAll` to evaluate.
 *
 * @example
 * `takeAll(range(5); //=> [0, 1, 2, 3, 4, 5]
 takeAll(range(-5, 2, -2); //=> [2, 0, -2, -4]
 takeAll(range(2, -2)); //=> [-2, -1, 0, 1]
 take(5, range(Infinity); //=> [0, 1, 2, 3, 4]`
 *
 * @param {Number} end - end of sequences
 * @param {Number} [start=0] - start of sequences
 * @param {Number} [step=0] - step between each element in range
 * @return {Function} - function to generate the sequences
 * @throws where all arg isn't Number type
 */

const range = (end, start, step) => {

  if (!isNum(end)) throw new Error('total arg must be an Number Type');
  if ((start || start === 0) && !isNum(start)) throw new Error('start arg must be an Number Type');
  if ((step || step === 0) && !isNum(step)) throw new Error('step arg must be an Number Type');

  const com = step < 0 ? (a, b) => a > b : (a, b) => a <b;
  
  function* _range(comparator, _end, _start = 0, _step = 1) {
    for (let i = _start; comparator(i,_end); i += _step ) {
      yield i;
    }
  }

  const gen = _range(com, end, start, step);
  return () => gen.next().value;
};



/**
 * return a function that will evaluate fn and optional args repeatedly n times.
 * 
 * @param {Number} n - time to repeat
 * @param {Function} fn - function to execute repeatedly
 * @param {...any} [args] - optional args to supply the function
 * @return {Function} function that wrap `Generator` object
 * @throws throws error if first argument isn't number type or second argument isn't function type
 */

const repeatedly = (n, fn, ...args) => {
  if (!isNum(n)) throw new Error('first arg must be an Number Type');
  if (!isFn(fn)) throw new Error('second arg must be function type');
  function* _repeatedly(_n, _fn, ..._args) {
    for (let x = 0; x < _n; x++){
      yield _fn(..._args);
    }
  }
  const gen = _repeatedly(n, fn, ...args);
  return () => gen.next().value;
};


/**
 * take an elements from arg from the left side (index 0).
 * 
 * @param {Number} n - number of elements to take
 * @param {(Object | Array | String | Generator)} arg - where to take the elements
 * @return {Array} elements taken
 * @throws if first arg isn't a Number type
 */


const take = (n, arg) => {
  if (!isNum(n)) throw new Error('First argument must Number type');
  if (isFn(arg)) {
    const result = [];
    for (let x = 0; x < n; x++) {
      const evaluated = arg();
      if (evaluated === undefined) break;
      result.push(evaluated);
    }
    return result;
  }

  if (isColl(arg)) {
    return iterable(arg).slice(0, n);
  }

  if (isGen(arg)){
    const result = [];
    for (let x = 0; x < n; x++){
      const evaluated = arg.next().value;
      if (evaluated === undefined) break;
      result.push(evaluated);
    }
    return result;
  }

  return [];
};


/**
 * drop an elements from arg from the left side (index 0).
 * 
 * @param {Number} n - number of elements to drop
 * @param {(Object | Array | String | Generator)} arg - where to drop the elements
 * @return {Array} elements not dropped
 * @throws if first arg isn't a Number type
 */

const drop = (n, arg) => { 
  if (!isNum(n)) throw new Error('First argument must Number type');
  if (isFn(arg) || isGen(arg)) {
    const allElems = takeAll(arg);
    return n === 0 ? allElems : allElems.slice(n, count(allElems));
  }

  if (isColl(arg)) {
    const iter = iterable(arg);
    return n === 0 ? iter : iter.slice(n, count(arg));
  }

  return [];
};


/**
 * same as `take` but take all element found in nth `n` position instead.
 * 
 * @param {Number} n - nth position
 * @param {(Object | Array | String)} coll - where to take the elements
 * @return {Array} elements taken
 * @throws if first arg isn't a Number type
 */

const takeNth = (n, coll) => {
  if (!isNum(n)) throw new Error('First argument must Number type');
  const iter = iterable(coll);
  const length = count(iter);
  if (n === 1) {
    return iter;
  }

  if (n > 1 && length > n) {
    const result = [];
    for (let x = n ; x <= length; x += n) {
      result.push(iter[x - 1]);
    }
    return result;
  }

  if (n === length){
    return [last(iter)];
  }

  return [];
};



/**
 * same as `drop` but drop all element found in nth `n` position instead.
 * 
 * @param {Number} n - nth position
 * @param {(Object | Array | String)} coll - where to drop the elements
 * @return {Array} elements not dropped
 * @throws if first arg isn't a Number type
 */

const dropNth = (n, coll) => {
  if (!isNum(n)) throw new Error('First argument must Number type');
  const length = count(coll);
  if (n === 1) {
    return [];
  }

  if (n > 1 && length > n) {
    let result = [];
    for (let x = 0; x < length; x += n) {
      result = [...result, ...take(n - 1, drop(x, coll))];
    }
    return result;
  }

  if (n === length){
    return [first(iterable(coll))];
  }

  return iterable(coll);
};

/**
 * repeatedly call a function. execute function supllied with optional args. mutual recursive call without consume a lot of stack
 * if the return value of `f` is function type, keep execute it until the return value not a function type,
 * 
 * @param {Function} f - function to execute
 * @param {...any} [args] - arg to supply to the function
 * @return {any} returned value from execute function `f`
 */


const trampoline = (f, ...args) => {
  const res = f(...args);
  if (isFn(res)) {
    let currentFn = res;
    let run = true;
    while (run) {
      currentFn = currentFn();
      if (!isFn(currentFn)) {
	return currentFn;
      }
    }
  }

  return res;
};


/**
 * assoc[iates] `k`, `v`, and/or `kvs` into an Object or create a new Object when first arg is not supllied.
 * if key exist, will replace with the latter.
 * 
 * @private
 * @function int_assocObj
 * @param {Object} obj - object to insert a value
 * @param {any} k - key to assoc
 * @param {any} v - value to assoc
 * @param {...any} [...kvs] - key-value pair sequence to assoc
 * @return {Object} object with inserted value 
 */

const int_assocObj = (obj, k, v, ...kvs) => {
  if (isEmpty(kvs)){
    return {...obj, [k]: v};
  }

  if (isOdd(kvs.length)) throw new Error('keys and values sequence must be even');
  return trampoline(int_assocObj, {...obj, [k]: v}, ...kvs);
};


/**
 * assoc[iates] `k`, `v`, and/or `kvs` into an Array or create a new Array when first arg is not supllied.
 * `k` is index of array to assoc, and `v` is the value at that index.
 * if index exist, will replace with the latter.
 * 
 * @private
 * @function int_assocObj
 * @param {Array} arr - Array to insert a value
 * @param {any} k - index as key to assoc
 * @param {any} v - value to assoc
 * @param {...any} [...kvs] - key-value pair sequence to assoc
 * @return {Array} object with inserted value 
 */

const int_assocArr = (arr, k, v, ...kvs) => {
  const newKvs = [k, v, ...kvs];
  if (isOdd(newKvs.length)) throw new Error('keys and values sequence must be even');
  const newObj = [...arr];
  const indexes = dropNth(2, newKvs);
  const values = takeNth(2, newKvs);
  const length = indexes.length;
  for (let x = 0; x < length; x++) {
    newObj.splice(indexes[x], 1, values[x]);
  }
  return newObj;
};


/**
 * insert a val by a key into an obj. if obj is Array type, key must be an index. if obj isn't supplied create a new Object.
 * 
 * @param {Object | Array} obj - object to insert a value
 * @param {...any} k, v, ..kvs - keys and values sequence to insert into
 * @return {(Object | Array)} object with inserted value 
 */

const assoc = (obj, k, v, ...kvs) => {
  return isArr(obj)
    ? int_assocArr(obj, k, v, ...kvs)
    : int_assocObj(obj, k, v, ...kvs);
};


/**
 * insert or replace a value in nested obj denoted by path. if key not found, a new Object will be created.
 * 
 * @param {Object | Array} obj - object to insert a value
 * @param {Array} path - keys path to put the value
 * @param {any} val - value to putting on
 * @return {(Object | Array)} object with inserted value
 */

const assocIn = (obj, [k, ...ks], val) => {
  if (isEmpty(ks)) {
    return assoc(obj, k, val);
  }

  return assoc(obj, k, trampoline(assocIn, get_(obj, k), ks, val));
};



/**
 * execute a set of pred and expression consecutively. 
 * if pred evaluated with arg return truthy, evaluate expression with the arg.
 *
 * @example
 * `condNext(1,
 [isEven, arg => arg + 1],
 [isOdd,  arg => arg + 10],
 [isNum,  arg => arg * 100]
 ); //=> 1100`
 *
 * @example 
 * `condNext(2,
 [isEven, arg => arg + 1],
 [isOdd,  arg => arg + 10],
 [isNum,  arg => arg * 100]
 ); //=> 1300`
 *
 * @param {any} arg - argument to evaluate
 * @param (...Array) setExpr - pred and expression pairs like [pred expression].
 * @return {any} result of evaluate the arg
 */

const condNext = (arg, ...setExpr) => {
  return reduce((init, [pred, expression]) => {
    if (isFn(pred) ? pred() : pred) {
      return expression(init);
    }

    return init;
  }, setExpr, arg);
};


/**
 * wrap function with callback, returning function when evaluating with args returning Promise
 *
 * @function promiseable
 * @param {Function} f - function does have callback
 * @return {Function} - function to apply args, returning Promise
 */

const promiseable = f => (...args) => {
  return new Promise((resolve, reject) => {
    const callback = (fargs, ...res) => {
      if ((fargs instanceof Error) || (last(res) instanceof Error)) {
	reject(fargs);
	return;
      }
      
      if (fargs && (res.length < 1) && !(fargs instanceof Error)){
	resolve(fargs);
	return;
      }

      if (!fargs && res) {
	resolve(res);
	return;
      }

      resolve([fargs, ...res]);
    };
    f(...args, callback);
  });
};


/**
 * create a function from method of Object
 *
 * @function execMethod
 * @param {Object} object - object does have method
 * @param {string} method - method to call
 * @return {Function} - function to apply args
 */

const execMethod = (object, method) => {
  return (...args) => {
    return object[method].call(object, ...args);
  };
};


/**
 * concatenate args into string
 *
 * @param {...any} ...any - args to concatenate into string
 * @return {String} result of concatenating args
 */

const str = (...any) => {
  return any.join('');
};


/**
 * flatten the (maybe deep) array
 *
 * @param {Array} arr - array to flatten
 * @return {Array} flattened array
 */

const flatten = arr => {
  return !isArr(arr)
    ? []
    : reduce((init, next) => isArr(next) ? [...init, ...trampoline(flatten, next)] : [...init, next], arr, []);
};

/**
 * merge `obj1` with maybe Object `obj2` using spread operator.
 * if `obj2` is Array will be converted Object by calling `reObj` function.
 * note: vector must be vector of pair
 *
 * @private
 * @function int_mergeObj
 * @param {Object} obj1 - first to merge
 * @param {(Object | Array)} obj2 second to merge
 * @return {(Object | null | undefined)} object with `obj1` and `obj2` merged or null or undefined
 * @throw error when second arg is not one of `Object`, `null`, `undefined`
 */

const int_mergeObj = (obj1, obj2) => {
  // if (!obj1) return obj2;
  if (!obj2) return obj1;

  if (isObj(obj2, true)) {
    return {...obj1, ...obj2};
  }

  if (isArr(obj2)) {
    return {...obj1, ...reObj(Object.entries(obj2))};
  }

  throw new Error("second arg must be Array or Object type");
  // return {...obj1, null: obj2};
};


/**
 * merge `arr1` with maybe array `arr2` using spread operator.
 * if `arr2` is Object will be converted into vector of pairs.
 * note: string treated as an Array of char.
 *
 * @private
 * @function int_mergeArr
 * @param {Array} arr1 - first to merge
 * @param {(Array | Object)} [arr2=[]] second to merge
 * @return {(Array | null | undefined)} array with `arr1` and `arr2` merged or null or undefined
 */

const int_mergeArr = (arr1, arr2) => {
  // if (!arr1) return arr2;
  if (!arr2) return arr1;

  return isArr(arr2) || isObj(arr2, true) ? [...arr1, ...iterable(arr2)] : [...arr1, arr2];
};


/**
 * merge collections, the first concrete type collection will be the final type
 * 
 * @function merge
 * @param {(Object | Array | null | undefined)} `coll1` - first to merge
 * @param {(Object | Array | null | undefined)} `coll2` - second to merge
 * @param {...(Object | Array | null | undefined)} `colln` - the rest to merge
 * @throw if first arg is not one of `Object`, `null`, `undefined`
 */

const merge = (coll1, coll2, ...colln) => {
  if (!coll1) return trampoline(merge, coll2, ...colln);
  if (!isObj(coll1, true) && !isArr(coll1)) throw new Error('First arg must be Object or Array type');
  if (isObj(coll1, true)){
    return isEmpty(colln) ? int_mergeObj(coll1, coll2) : trampoline(merge, int_mergeObj(coll1, coll2), ...colln);
  }

  return isEmpty(colln) ? int_mergeArr(coll1, coll2) : trampoline(merge, int_mergeArr(coll1, coll2), ...colln);
};

/**
 * merge `obj1` with maybe Object `obj2` using spread operator.
 * if `obj2` is Array will be converted Object by calling `reObj` function.
 *
 * @private
 * @function int_mergeObjWith
 * @param {Function} fn - function to apply when encountering same key
 * @param {Object} obj1 - first to merge
 * @param {(Object | Array)} obj2 second to merge
 * @return {(Object | null | undefined)} object with `obj1` and `obj2` merged or null or undefined
 * @throw error when second arg is not one of `Object`, `null`, `undefined`
 */

const int_mergeObjWith = (fn, obj1, obj2) => {
  if (!obj2) return obj1;

  if (isObj(obj2, true)) {
    return reduce((init, [k, v]) => {
      if (init[k]) return assoc(init, k, fn(init[k], v));
      return assoc(init, k, v);
    }, obj2, obj1);
  }

  if (isArr(obj2)) return int_mergeObjWith(fn, obj1, reObj(Object.entries(obj2)));

  throw new Error("second arg must be Array or Object type");
};

/**
 * merge collections by applying `fn` when encountering same key, fn must accept 2 args like `fn(old, new)`
 * the first concrete type collection will be the final type
 * 
 * @function mergeWith
 * @param {Function} fn - function to apply when encountering same key
 * @param {(Object | Array | null | undefined)} `coll1` - first to merge
 * @param {(Object | Array | null | undefined)} `coll2` - second to merge
 * @param {...(Object | Array | null | undefined)} `colln` - the rest to merge
 * @throw if first arg is not one of `Object`, `null`, `undefined`
 */

const mergeWith = (fn, coll1, coll2, ...colln) => {
  if (!isFn(fn)) throw new Error('First arg must be Function type');
  if (!coll1) return trampoline(mergeWith, fn, coll2, ...colln);

  if (isObj(coll1, true)){
    return isEmpty(colln) ? int_mergeObjWith(fn, coll1, coll2) : trampoline(mergeWith, fn, int_mergeObjWith(fn, coll1, coll2), ...colln);
  }

  if (isArr(coll1)) {
    return isEmpty(colln) ? int_mergeArr(coll1, coll2) : merge(int_mergeArr(coll1, coll2), ...colln);
  }
  
  throw new Error('First arg must be Object or Array type');
};

/**
 * function to apply with `mergeWith` recursively merge object
 *
 * @private
 * @function int_deepMergeX
 * @param {any} o - old value
 * @param {any} n - new value
 * @return {any} selected value
 */

const int_deepMergeX = (o, n) => {
  if (!n) return o;
  if (isObj(o, true) && isObj(n, true)) return trampoline(deepMerge, o, n);
  if (isArr(o) && isArr(n)) return int_mergeArr(o, n);
  return n;
};


/**
 * merge collections deeply
 * 
 * @function deepMerge
 * @param {(Object | Array | null | undefined)} `coll1` - first to merge
 * @param {(Object | Array | null | undefined)} `coll2` - second to merge
 * @param {...(Object | Array | null | undefined)} `colln` - the rest to merge
 * @throw all throwable found in `mergeWith`
 */

const deepMerge = (coll1, coll2, ...colln) => {
  if (isEmpty(colln)) {
    return mergeWith(int_deepMergeX, coll1, coll2);
  }
  return trampoline(mergeWith, int_deepMergeX, mergeWith(int_deepMergeX, coll1, coll2), ...colln);
};


/**
 * insert or update a value in nested obj denoted by path. if key not found, a new Object will be created.
 * 
 * @param {Object | Array} obj - object to insert a value
 * @param {Array} path - keys path to put the value
 * @param {Function} fn - function to apply to the updated path, will be `apply(fn, old, ..args)`
 * @param {...any} vals - args to apply to the `fn` with old value
 * @return {(Object | Array)} object with inserted value
 * @throw if `fn` is not function
 */

const updateIn = (obj, [k, ...ks], fn, ...vals) => {
  if (!isFn(fn)) throw new Error(`${fn} should be function type`);
  if (!isEmpty(ks)) {
    return assoc(obj, k, trampoline(updateIn, get_(obj, k), ks, fn, ...vals));
  }

  return assoc(obj, k, fn(get_(obj, k), ...vals));
};


/**
 * return true if array is equal
 * 
 * @private
 * @function int_arrEquality
 * @param {Array} arr1 - first arg to check
 * @param {Array} arr2 - second arg to check
 * @return {boolean} - true if `arr1` and `arr2` is equal
 */

const int_arrEquality = (arr1, arr2) => {
  if (arr1.length !== arr2.length) return false;
  for (let x = 0; x < arr1.length; x++){
    const arr1Elem = arr1[x];
    const arr2Elem = arr2[x];
    if (isArr(arr1Elem) && isArr(arr2Elem)) return trampoline(int_arrEquality, arr1Elem, arr2Elem);
    if (isObj(arr1Elem) && isObj(arr2Elem)) return trampoline(int_objEquality, arr1Elem, arr2Elem);
    if (arr1Elem !== arr2Elem) return false;
  }
  return true;
};


/**
 * return true if object is equal
 * 
 * @private
 * @function int_objEquality
 * @param {Object} obj1 - first arg to check
 * @param {Object} obj2 - second arg to check
 * @return {boolean} - true if `arr1` and `arr2` is equal
 */

const int_objEquality = (obj1, obj2) => {
  if (count(obj1) !== count(obj2)) return false;
  for (const k in obj1){
    const obj1Elem = obj1[k];
    const obj2Elem = obj2[k];
    if (isArr(obj1Elem) && isArr(obj2Elem)) return trampoline(int_arrEquality, obj1Elem, obj2Elem);
    if (isObj(obj1Elem) && isObj(obj2Elem)) return trampoline(int_objEquality, obj1Elem, obj2Elem);
    if (obj1Elem !== obj2Elem) return false;
  }
  return true;
};


/**
 * do comparison of `a` and `b`, do recursively if possible
 *
 * @param {any} a -  thing to check
 * @param {any} b - thing to check
 * @return {boolean} - true if `a` and `b`
 */

const isEqual = (a, b) => {
  if (isArr(a) && isArr(b)) return int_arrEquality(a, b);
  if (isObj(a) && isObj(b)) return int_objEquality(a, b);
  return a === b;
};


/**
 * return position of `elem` in `arr` or null if not found
 *
 * @param {Array} arr - where to check
 * @param {any} elem - what to check
 * @return {(Array | null)} array will be `[index, elem]`
 */

const pos = (arr, elem) => {
  if (!isArr(arr)) throw new Error('Arg must an Array type');
  for (let x = 0; x < arr.length; x++) {
    if (isEqual(arr[x], elem)) return [x, elem];
  }
  return null;
};


/**
 * do deduplication of elem in array
 *
 * @param {Array} arr - array to deduplication
 * @return {Array} new array with unique value
 */

const dedupe = arr => {
  if (isArr(arr)) {
    return reduce((init, next) => {
      return (!pos(init, next)) ? [...init, next] : init;
    }, arr, []);
  } else {
    return arr;
  }
};


module.exports.partial = partial;
module.exports.evaluate = evaluate;
module.exports.doAll = doAll;
module.exports.constantly = constantly;
module.exports.identity = identity;
module.exports.compose = compose;
module.exports._compose = _compose;
module.exports.map = map;
module.exports.reduce = reduce;
module.exports.iterable = iterable;
module.exports.groupBy = groupBy;
module.exports.freq = freq;
module.exports.zipMap = zipMap;
module.exports.selectKeys = selectKeys;
module.exports.dropKeys = dropKeys;
module.exports.clone = clone;
module.exports.getIn = getIn;
module.exports.isFn = isFn;
module.exports.isArr = isArr;
module.exports.isObj = isObj;
module.exports.isStr = isStr;
module.exports.isColl = isColl;
module.exports.count = count;
module.exports.isEmpty = isEmpty;
module.exports.first = first;
module.exports.last = last;
module.exports.butLast = butLast;
module.exports.rest = rest;
module.exports.apply = apply;
module.exports.isGen = isGen;
module.exports.isNum = isNum;
module.exports.isOdd = isOdd;
module.exports.isEven = isEven;
module.exports.reObj = reObj;
module.exports.get_ = get_;
module.exports.range = range;
module.exports.takeAll = takeAll;
module.exports.repeatedly = repeatedly;
module.exports.take = take;
module.exports.drop = drop;
module.exports.takeNth = takeNth;
module.exports.dropNth = dropNth;
module.exports.trampoline = trampoline;
module.exports.assoc = assoc;
module.exports.assocIn = assocIn;
module.exports.condNext = condNext;
module.exports.merge = merge;
module.exports.dedupe = dedupe;
module.exports.pos = pos;
module.exports.isEqual = isEqual;
module.exports.updateIn = updateIn;
module.exports.flatten = flatten;
module.exports.execMethod = execMethod;
module.exports.str = str;
module.exports.promiseable = promiseable;
module.exports.mergeWith = mergeWith;
module.exports.deepMerge = deepMerge;
