'use strict';

const {reduce, isNum, isEmpty} = require('./functionals.js');


const _add = (a, b) => a + b;
const _sub = (a, b) => a - b;
const _div = (a, b) => a / b;
const _mult = (a, b) => a * b;
const _mod = (a, b) => a % b;

const mathable = (operator, oper1, oper2) => {
  if (!isNum(oper1)) throw new Error(`${JSON.stringify(oper1)} is not a Number type`);
  if (!isNum(oper2)) throw new Error(`${JSON.stringify(oper2)} is not a Number type`);
  const result = operator(oper1, oper2);
  if (!isNum(result)) throw new Error(`result ${result} is not a Number type`); 
  return result;
};


const mathableExInf = (operator, oper1, oper2) => {
  if (!isNum(oper1) || oper1 === Infinity) throw new Error(`${JSON.stringify(oper1)} is not a Number type or an Infinity`);
  if (!isNum(oper2) || oper2 === Infinity) throw new Error(`${JSON.stringify(oper2)} is not a Number type or an Infinity`);
  const result = operator(oper1, oper2);
  if (!isNum(result)) throw new Error(`result ${result} is not a Number type`);
  if (result === Infinity) throw new Error('result is an Infinity, maybe you divide by zero?');
  return result;
};


const seqMathOps = (operator, operands) => {
  if (isEmpty(operands)) return 0;
  if (operands.length === 1) {
    if (isNum(operands[0])) {
      return operands[0];
    } else {
      throw new Error('arguments must be sequence of Number Type');
    }
  } else {
    return reduce((init, next) => {
      return mathable(operator, init, next);
    }, operands);
  }
};


const seqMathOpsNoInf = (operator, operands) => {
  if (isEmpty(operands)) return 0;
  if (operands.length === 1) {
    if (isNum(operands[0]) && operands[0] !== Infinity) {
      return operands[0];
    } else {
      throw new Error('arguments must be sequence of Number Type. Infinity not allowed');
    }
  } else {
    return reduce((init, next) => {
      return mathableExInf(operator, init, next);
    }, operands);
  }
};


module.exports = {
  add: (...numbers) => seqMathOps(_add, numbers),
  sub: (...numbers) => seqMathOps(_sub, numbers),
  div: (...numbers) => seqMathOps(_div, numbers),
  mult: (...numbers) => seqMathOps(_mult, numbers),
  mod: (...numbers) => seqMathOps(_mod, numbers),
  noInf: {
    add: (...numbers) => seqMathOpsNoInf(_add, numbers),
    sub: (...numbers) => seqMathOpsNoInf(_sub, numbers),
    div: (...numbers) => seqMathOpsNoInf(_div, numbers),
    mult: (...numbers) => seqMathOpsNoInf(_mult, numbers),
    mod: (...numbers) => seqMathOpsNoInf(_mod, numbers),
  }
};
