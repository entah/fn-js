[![coverage](https://gitlab.com/entah/fn-js/badges/master/coverage.svg)](https://gitlab.com/entah/fn-js)
[![pipeline](https://gitlab.com/entah/fn-js/badges/master/pipeline.svg)](https://gitlab.com/entah/fn-js)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=fn-js&metric=alert_status)](https://sonarcloud.io/dashboard?id=fn-js)

## Collection of functions inspired by [Clojure[script]](https://clojure.org)

## Rationale

I really loved to tinkering using Clojure[script], so i want to introduce the same feels in Javascript.

This sets of function is my journey to understand javascript world.

More will be added soon.


## API Docs

Generate one using `yarn doc`. The ouput is on `docs/generated/` dir.

You can visit API Docs pages [here](https://entah.gitlab.io/fn-js/).

The API Docs build by [jsdoc](https://jsdoc.app)

## How to use

Clone this repository using 
``` bash
git clone https://gitlab.com/entah/fn-js.git
```

### Raw
You can just grab file under `src/` directory, since i'm not using any library for core funtional.

### Node JS
You can add entry `"fn-js": "https://gitlab.com/entah/fn-js.git"` into your dependencies map and run `yarn install` or `npm install`.

Or, you can add a dependencies using yarn command `yarn add 'https://gitlab.com/entah/fn-js.git#master'`

Now, fn-js can be downloaded from npm public repository by using:
``` bash
yarn add @entah/fn-js

# or

npm i @entah/fn-js
```

### Browser
You can build the file for browser using `yarn build`.

I'm using [webpack](https://webpack.js.org/) for build the file.


## NOTE

1. ~~This sets still under heavy development. Consider it for not use this on production.~~
2. Due to `Object` is **mutable**. I recommended you to take a note when use this sets of function with **globally declared** `Object`/`Array`. All function in this sets will try to clone the input (if `Object` or `Array`) using `spread operator` (first depth copy) and do the same for the output. Look at below snippet:


	```javascript

	const {groupBy, identity, clone} = require('fn-js');

	const foo a = [1, 2, 3, 1, 2, 4];

	const groupedFoo = groupBy(identity, foo); //=> {'1': [1, 1], '2': [2, 2], '3': [3], '4': [4]}

	// if we try to change foo element, it's okay since foo only 1 depth

	foo[0] = 100;

	console.log(foo); //=> [100, 2, 3, 1, 2, 4]
	console.log(groupedFoo) //=> {'1': [1, 1], '2': [2, 2], '3': [3], '4': [4]}

	// but becareful for nested Object or Array

	const bar = [[1, 2, 2], [1, 2, 2], [3, 3, 3], [3, 4, 4]];

	const groupedBar = groupBy(arg => arg[0], bar); //=> {'1': [[1, 2, 2], [1, 2, 2]], '3': [[3, 3, 3,], [4, 4, 4]]}

	// let's mutate the the second element of second entry
	bar[1][1] = 100;


	console.log(bar); //=> [[1, 2, 2], [1, 100, 2], [3, 3, 3], [3, 4, 4]]
	console.log(groupedBar); //=> {'1': [[1, 2, 2], [1, 100, 2]], '3': [[3, 3, 3,], [3, 4, 4]]}

	// as you can see, the result was changed due to 'deep structure reference'.


	// to play around of this problem, we can use clone.
	// clone will traverse into bottom of nested Object/Array, so there is kind of trade-off to consider.

	const baz = [[1, 2, 2], [1, 2, 2], [3, 3, 3], [3, 4, 4]];

	const groupedBaz = groupBy(arg => arg[0], clone(baz)); //=> {'1': [[1, 2, 2], [1, 2, 2]], '3': [[3, 3, 3,], [3, 4, 4]]}

	// let's mutate the the second element of second entry
	baz[1][1] = 100;


	console.log(baz); //=> [[1, 2, 2], [1, 100, 2], [3, 3, 3], [3, 4, 4]]
	console.log(groupedBaz); //=> {'1': [[1, 2, 2], [1, 2, 2]], '3': [[3, 3, 3,], [3, 4, 4]]}

	// the result not changed

	```


3. This sets of functions treat `Object` as map-like key-value-pair. So, `Map` will be not work.
4. The function name `_[name_of_function]` mean it's prefer use in asynchronous function, `[name_of_function]_` mean the output isn't cloned, and the rest is for synchronous functions.


## TODO

1. Create tutorial for using a function introduce in this sets
2. Optimize trampoline function.
3. Using `mutual recursion` for loop-like operation to make sure no local variable get mutated
4. ~~Create another repo to show a real world use of this set of function~~ you can check on [res2an](https://gitlab.com/entah/rest2an)
5. Introduces `Map` and `Set`?


## Versioning

This project will use semantic versioning.
