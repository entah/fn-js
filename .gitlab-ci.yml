# You can override the included template(s) by including variable overrides
# See https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#priority-of-environment-variables
image: registry.gitlab.com/meyer-docker-dev/lambda-node-builder:latest

variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
  GIT_DEPTH: '0'

stages:
  - test
  - code-quality
  - deploy
  - publish

.config-npm-publish:
  before_script:
    - git config user.email "${EMAIL_DEPLOYER}"
    - git config user.name "${NAME_DEPLOYER}"
    - git remote set-url origin ${PROJECT_URL/gitlab.com/oauth2:${ACCESS_TOKEN}@gitlab.com}.git
    - npm config set "//registry.npmjs.org/:_authToken=${NPM_DEPLOY_TOKEN}"
  when: manual
  only:
    - release
    - merge_requests
  allow_failure: true

.config-npm-cache:
  cache:
    key:
      files:
        - package.json
    paths:
      - node_modules
    untracked: true

test_package:
  stage: test
  extends:
    - .config-npm-cache
  before_script:
  - yarn
  coverage: "/Statements\\s+:\\s?(.+\\%).+$/"
  script:
  - yarn ci
  artifacts:
    name: "$CI_COMMIT_REF_NAME"
    paths:
    - coverage
    expire_in: 5 mins

sonarcloud-check:
  stage: code-quality
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint:
    - ''
  cache:
    key: "${CI_JOB_NAME}"
    paths:
    - ".sonar/cache"
  allow_failure: true
  script:
  - sonar-scanner -Dsonar.qualitygate.wait=true

pages:
  stage: deploy
  extends:
    - .config-npm-cache
  before_script:
  - yarn
  - mkdir -p docs/tutorials
  - rm -rf public
  script:
  - yarn doc
  - mv docs/generated public
  artifacts:
    paths:
    - public
  only:
  - master
  - release
  allow_failure: true

sast:
  variables:
    SAST_DEFAULT_ANALYZERS: brakeman, bandit, eslint, flawfinder, kubesec, nodejs-scan,
      gosec, phpcs-security-audit, pmd-apex, security-code-scan, sobelow, spotbugs
  stage: test

publish_package_patch:
  stage: publish
  extends:
    - .config-npm-cache
    - .config-npm-publish
  script:
    - npm version patch                                             # bump patch version
    - git push origin HEAD:release --follow-tags --force -o ci.skip # we push back tag to remote server
    - npm publish --access public                                   # publish to npm public registry

publish_package_minor:
  stage: publish
  extends:
    - .config-npm-cache
    - .config-npm-publish
  script:
    - npm version minor                                             # bump minor version
    - git push origin HEAD:release --follow-tags --force -o ci.skip # we push back tag to remote server
    - npm publish --access public                                   # publish to npm public registry

publish_package_major:
  stage: publish
  extends:
    - .config-npm-cache
    - .config-npm-publish
  script:
    - npm version major                                             # bump major version
    - git push origin HEAD:release --follow-tags --force -o ci.skip # we push back tag to remote server
    - npm publish --access public                                   # publish to npm public registry

include:
  - template: Security/SAST.gitlab-ci.yml
