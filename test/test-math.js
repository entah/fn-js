const ava = require('ava');
const {add, sub, div, mult, mod, noInf: {add: addNI, sub: subNI, div: divNI, mult: multNI, mod: modNI}} = require('../src/maths.js');


ava('add function', t => {
    t.is(add(), 0, 'no arg return 0');
    t.is(add(0), 0, 'zero arg return 0');
    t.is(add(Infinity), Infinity, 'Infinity arg return Infinity');
    t.is(add(1), 1, 'return its arg where arg is num and not zero');
    t.is(add(1, 2, 3, 4), 10, 'sequencial arg return ops of all element');
    t.is(add(1, -2, 3, -4), -2, 'sequencial arg return ops of all element');
    t.is(add(-0, 0, 1, 2, 3, 4), 10, 'sequencial arg return ops of all element');
    t.is(add(1, 0), 1);
    t.is(add(0, 1), 1);
    t.throws(() => add(null));
    t.throws(() => add(undefined));
    t.throws(() => add(NaN));
    t.throws(() => add([]));
    t.throws(() => add({}));
    t.throws(() => add(new Date()));
    t.throws(() => add(new RegExp('')));
    t.throws(() => add(1, null));
    t.throws(() => add(1, undefined));
    t.throws(() => add(1, NaN));
    t.throws(() => add(1, new Date()));
    t.throws(() => add(1, new RegExp('')));
    t.throws(() => add(1, null, 2));
    t.throws(() => add(1, undefined, 2));
    t.throws(() => add(1, NaN, 2));
    t.throws(() => add(1, new Date(), 2));
    t.throws(() => add(1, new RegExp(''), 2));
    t.throws(() => add(1, null));
    t.throws(() => add(1, undefined));
    t.throws(() => add(1, NaN));
    t.throws(() => add(1, new Date()));
    t.throws(() => add(1, new RegExp('')));
    t.throws(() => add(null, 1));
    t.throws(() => add(undefined, 1));
    t.throws(() => add(NaN, 1));
    t.throws(() => add(new Date(), 1));
    t.throws(() => add(new RegExp(''), 1));
});

ava('sub function', t => {
    t.is(sub(), 0, 'no arg return 0');
    t.is(sub(0), 0, 'zero arg return 0');
    t.is(sub(Infinity), Infinity, 'Infinity arg return Infinity');
    t.is(sub(1), 1, 'return its arg where arg is num and not zero');
    t.is(sub(1, 2, 3, 4), -8, 'sequencial arg return ops of all element');
    t.is(sub(1, -2, 3, -4), 4, 'sequencial arg return ops of all element');
    t.is(sub(-0, 0, 1, 2, 3, 4), -10, 'sequencial arg return ops of all element');
    t.is(sub(1, 0), 1);
    t.is(sub(0, 1), -1);
    t.throws(() => sub(null));
    t.throws(() => sub(undefined));
    t.throws(() => sub(NaN));
    t.throws(() => sub([]));
    t.throws(() => sub({}));
    t.throws(() => sub(new Date()));
    t.throws(() => sub(new RegExp('')));
    t.throws(() => sub(1, null));
    t.throws(() => sub(1, undefined));
    t.throws(() => sub(1, NaN));
    t.throws(() => sub(1, new Date()));
    t.throws(() => sub(1, new RegExp('')));
    t.throws(() => sub(1, null, 2));
    t.throws(() => sub(1, undefined, 2));
    t.throws(() => sub(1, NaN, 2));
    t.throws(() => sub(1, new Date(), 2));
    t.throws(() => sub(1, new RegExp(''), 2));
    t.throws(() => sub(1, null));
    t.throws(() => sub(1, undefined));
    t.throws(() => sub(1, NaN));
    t.throws(() => sub(1, new Date()));
    t.throws(() => sub(1, new RegExp('')));
    t.throws(() => sub(null, 1));
    t.throws(() => sub(undefined, 1));
    t.throws(() => sub(NaN, 1));
    t.throws(() => sub(new Date(), 1));
    t.throws(() => sub(new RegExp(''), 1));
});


ava('mult function', t => {
    t.is(mult(), 0, 'no arg return 0');
    t.is(mult(0), 0, 'zero arg return 0');
    t.is(mult(Infinity), Infinity, 'Infinity arg return Infinity');
    t.is(mult(1), 1, 'return its arg where arg is num and not zero');
    t.is(mult(1, 2, 3, 4), 24, 'sequencial arg return ops of all element');
    t.is(mult(1, -2, 3, -4), 24, 'sequencial arg return ops of all element');
    t.is(mult(-0, 0, 1, 2, 3, 4), -0, 'sequencial arg return ops of all element');
    t.is(mult(1, 0), 0);
    t.is(mult(0, 1), 0);
    t.throws(() => mult(null));
    t.throws(() => mult(undefined));
    t.throws(() => mult(NaN));
    t.throws(() => mult([]));
    t.throws(() => mult({}));
    t.throws(() => mult(new Date()));
    t.throws(() => mult(new RegExp('')));
    t.throws(() => mult(1, null));
    t.throws(() => mult(1, undefined));
    t.throws(() => mult(1, NaN));
    t.throws(() => mult(1, new Date()));
    t.throws(() => mult(1, new RegExp('')));
    t.throws(() => mult(1, null, 2));
    t.throws(() => mult(1, undefined, 2));
    t.throws(() => mult(1, NaN, 2));
    t.throws(() => mult(1, new Date(), 2));
    t.throws(() => mult(1, new RegExp(''), 2));
    t.throws(() => mult(1, null));
    t.throws(() => mult(1, undefined));
    t.throws(() => mult(1, NaN));
    t.throws(() => mult(1, new Date()));
    t.throws(() => mult(1, new RegExp('')));
    t.throws(() => mult(null, 1));
    t.throws(() => mult(undefined, 1));
    t.throws(() => mult(NaN, 1));
    t.throws(() => mult(new Date(), 1));
    t.throws(() => mult(new RegExp(''), 1));
});

ava('div function', t => {
    t.is(div(), 0, 'no arg return 0');
    t.is(div(0), 0, 'zero arg return 0');
    t.is(div(Infinity), Infinity, 'Infinity arg return Infinity');
    t.is(div(1), 1, 'return its arg where arg is num and not zero');
    t.is(div(1, 2, 3, 4), 0.041666666666666664, 'sequencial arg return ops of all element');
    t.is(div(1, -2, 3, -4), 0.041666666666666664, 'sequencial arg return ops of all element');
    t.is(div(1, 0), Infinity);
    t.is(div(0, 1), 0);
    t.throws(() => div(-0, 0, 1, 2, 3, 4));
    t.throws(() => div(null));
    t.throws(() => div(undefined));
    t.throws(() => div(NaN));
    t.throws(() => div([]));
    t.throws(() => div({}));
    t.throws(() => div(new Date()));
    t.throws(() => div(new RegExp('')));
    t.throws(() => div(1, null));
    t.throws(() => div(1, undefined));
    t.throws(() => div(1, NaN));
    t.throws(() => div(1, new Date()));
    t.throws(() => div(1, new RegExp('')));
    t.throws(() => div(1, null, 2));
    t.throws(() => div(1, undefined, 2));
    t.throws(() => div(1, NaN, 2));
    t.throws(() => div(1, new Date(), 2));
    t.throws(() => div(1, new RegExp(''), 2));
    t.throws(() => div(1, null));
    t.throws(() => div(1, undefined));
    t.throws(() => div(1, NaN));
    t.throws(() => div(1, new Date()));
    t.throws(() => div(1, new RegExp('')));
    t.throws(() => div(null, 1));
    t.throws(() => div(undefined, 1));
    t.throws(() => div(NaN, 1));
    t.throws(() => div(new Date(), 1));
    t.throws(() => div(new RegExp(''), 1));
});

ava('mod function', t => {
    t.is(mod(), 0, 'no arg return 0');
    t.is(mod(0), 0, 'zero arg return 0');
    t.is(mod(Infinity), Infinity, 'Infinity arg return Infinity');
    t.is(mod(1), 1, 'return its arg where arg is num and not zero');
    t.is(mod(1, 2, 3, 4), 1, 'sequencial arg return ops of all element');
    t.is(mod(1, -2, 3, -4), 1, 'sequencial arg return ops of all element');
    t.is(mod(0, 1), 0);
    t.throws(() => mod(1, 0));
    t.throws(() => mod(-0, 0, 1, 2, 3, 4));
    t.throws(() => mod(null));
    t.throws(() => mod(undefined));
    t.throws(() => mod(NaN));
    t.throws(() => mod([]));
    t.throws(() => mod({}));
    t.throws(() => mod(new Date()));
    t.throws(() => mod(new RegExp('')));
    t.throws(() => mod(1, null));
    t.throws(() => mod(1, undefined));
    t.throws(() => mod(1, NaN));
    t.throws(() => mod(1, new Date()));
    t.throws(() => mod(1, new RegExp('')));
    t.throws(() => mod(1, null, 2));
    t.throws(() => mod(1, undefined, 2));
    t.throws(() => mod(1, NaN, 2));
    t.throws(() => mod(1, new Date(), 2));
    t.throws(() => mod(1, new RegExp(''), 2));
    t.throws(() => mod(1, null));
    t.throws(() => mod(1, undefined));
    t.throws(() => mod(1, NaN));
    t.throws(() => mod(1, new Date()));
    t.throws(() => mod(1, new RegExp('')));
    t.throws(() => mod(null, 1));
    t.throws(() => mod(undefined, 1));
    t.throws(() => mod(NaN, 1));
    t.throws(() => mod(new Date(), 1));
    t.throws(() => mod(new RegExp(''), 1));
});


ava('addNI function', t => {
    t.is(addNI(), 0, 'no arg return 0');
    t.is(addNI(0), 0, 'zero arg return 0');
    t.is(addNI(1), 1, 'return its arg where arg is num and not zero');
    t.is(addNI(1, 2, 3, 4), 10, 'sequencial arg return ops of all element');
    t.is(addNI(1, -2, 3, -4), -2, 'sequencial arg return ops of all element');
    t.is(addNI(-0, 0, 1, 2, 3, 4), 10, 'sequencial arg return ops of all element');
    t.is(addNI(1, 0), 1);
    t.is(addNI(0, 1), 1);
    t.throws(() => addNI(Infinity));
    t.throws(() => addNI(null));
    t.throws(() => addNI(undefined));
    t.throws(() => addNI(NaN));
    t.throws(() => addNI([]));
    t.throws(() => addNI({}));
    t.throws(() => addNI(new Date()));
    t.throws(() => addNI(new RegExp('')));
    t.throws(() => addNI(1, null));
    t.throws(() => addNI(1, undefined));
    t.throws(() => addNI(1, NaN));
    t.throws(() => addNI(1, new Date()));
    t.throws(() => addNI(1, new RegExp('')));
    t.throws(() => addNI(1, null, 2));
    t.throws(() => addNI(1, undefined, 2));
    t.throws(() => addNI(1, NaN, 2));
    t.throws(() => addNI(1, new Date(), 2));
    t.throws(() => addNI(1, new RegExp(''), 2));
    t.throws(() => addNI(1, null));
    t.throws(() => addNI(1, undefined));
    t.throws(() => addNI(1, NaN));
    t.throws(() => addNI(1, new Date()));
    t.throws(() => addNI(1, new RegExp('')));
    t.throws(() => addNI(null, 1));
    t.throws(() => addNI(undefined, 1));
    t.throws(() => addNI(NaN, 1));
    t.throws(() => addNI(new Date(), 1));
    t.throws(() => addNI(new RegExp(''), 1));
});

ava('subNI function', t => {
    t.is(subNI(), 0, 'no arg return 0');
    t.is(subNI(0), 0, 'zero arg return 0');
    t.is(subNI(1), 1, 'return its arg where arg is num and not zero');
    t.is(subNI(1, 2, 3, 4), -8, 'sequencial arg return ops of all element');
    t.is(subNI(1, -2, 3, -4), 4, 'sequencial arg return ops of all element');
    t.is(subNI(-0, 0, 1, 2, 3, 4), -10, 'sequencial arg return ops of all element');
    t.is(subNI(1, 0), 1);
    t.is(subNI(0, 1), -1);
    t.throws(() => subNI(Infinity));
    t.throws(() => subNI(null));
    t.throws(() => subNI(undefined));
    t.throws(() => subNI(NaN));
    t.throws(() => subNI([]));
    t.throws(() => subNI({}));
    t.throws(() => subNI(new Date()));
    t.throws(() => subNI(new RegExp('')));
    t.throws(() => subNI(1, null));
    t.throws(() => subNI(1, undefined));
    t.throws(() => subNI(1, NaN));
    t.throws(() => subNI(1, new Date()));
    t.throws(() => subNI(1, new RegExp('')));
    t.throws(() => subNI(1, null, 2));
    t.throws(() => subNI(1, undefined, 2));
    t.throws(() => subNI(1, NaN, 2));
    t.throws(() => subNI(1, new Date(), 2));
    t.throws(() => subNI(1, new RegExp(''), 2));
    t.throws(() => subNI(1, null));
    t.throws(() => subNI(1, undefined));
    t.throws(() => subNI(1, NaN));
    t.throws(() => subNI(1, new Date()));
    t.throws(() => sub(1, new RegExp('')));
    t.throws(() => subNI(null, 1));
    t.throws(() => subNI(undefined, 1));
    t.throws(() => subNI(NaN, 1));
    t.throws(() => subNI(new Date(), 1));
    t.throws(() => subNI(new RegExp(''), 1));
});


ava('multNI function', t => {
    t.is(multNI(), 0, 'no arg return 0');
    t.is(multNI(0), 0, 'zero arg return 0');
    t.is(multNI(1), 1, 'return its arg where arg is num and not zero');
    t.is(multNI(1, 2, 3, 4), 24, 'sequencial arg return ops of all element');
    t.is(multNI(1, -2, 3, -4), 24, 'sequencial arg return ops of all element');
    t.is(multNI(-0, 0, 1, 2, 3, 4), -0, 'sequencial arg return ops of all element');
    t.is(multNI(1, 0), 0);
    t.is(multNI(0, 1), 0);
    t.throws(() => multNI(Infinity));
    t.throws(() => multNI(null));
    t.throws(() => multNI(undefined));
    t.throws(() => multNI(NaN));
    t.throws(() => multNI([]));
    t.throws(() => multNI({}));
    t.throws(() => multNI(new Date()));
    t.throws(() => multNI(new RegExp('')));
    t.throws(() => multNI(1, null));
    t.throws(() => multNI(1, undefined));
    t.throws(() => multNI(1, NaN));
    t.throws(() => multNI(1, new Date()));
    t.throws(() => multNI(1, new RegExp('')));
    t.throws(() => multNI(1, null, 2));
    t.throws(() => multNI(1, undefined, 2));
    t.throws(() => multNI(1, NaN, 2));
    t.throws(() => multNI(1, new Date(), 2));
    t.throws(() => multNI(1, new RegExp(''), 2));
    t.throws(() => multNI(1, null));
    t.throws(() => multNI(1, undefined));
    t.throws(() => multNI(1, NaN));
    t.throws(() => multNI(1, new Date()));
    t.throws(() => multNI(1, new RegExp('')));
    t.throws(() => multNI(null, 1));
    t.throws(() => multNI(undefined, 1));
    t.throws(() => multNI(NaN, 1));
    t.throws(() => multNI(new Date(), 1));
    t.throws(() => multNI(new RegExp(''), 1));
});


ava('divNI function', t => {
    t.is(divNI(), 0, 'no arg return 0');
    t.is(divNI(0), 0, 'zero arg return 0');
    t.is(divNI(1), 1, 'return its arg where arg is num and not zero');
    t.is(divNI(1, 2, 3, 4), 0.041666666666666664, 'sequencial arg return ops of all element');
    t.is(divNI(1, -2, 3, -4), 0.041666666666666664, 'sequencial arg return ops of all element');
    t.is(divNI(0, 1), 0);
    t.throws(() => divNI(1, 0));
    t.throws(() => divNI(Infinity));
    t.throws(() => divNI(-0, 0, 1, 2, 3, 4));
    t.throws(() => divNI(null));
    t.throws(() => divNI(undefined));
    t.throws(() => divNI(NaN));
    t.throws(() => divNI([]));
    t.throws(() => divNI({}));
    t.throws(() => divNI(new Date()));
    t.throws(() => divNI(new RegExp('')));
    t.throws(() => divNI(1, null));
    t.throws(() => divNI(1, undefined));
    t.throws(() => divNI(1, NaN));
    t.throws(() => divNI(1, new Date()));
    t.throws(() => divNI(1, new RegExp('')));
    t.throws(() => divNI(1, null, 2));
    t.throws(() => divNI(1, undefined, 2));
    t.throws(() => divNI(1, NaN, 2));
    t.throws(() => divNI(1, new Date(), 2));
    t.throws(() => divNI(1, new RegExp(''), 2));
    t.throws(() => divNI(1, null));
    t.throws(() => divNI(1, undefined));
    t.throws(() => divNI(1, NaN));
    t.throws(() => divNI(1, new Date()));
    t.throws(() => divNI(1, new RegExp('')));
    t.throws(() => divNI(null, 1));
    t.throws(() => divNI(undefined, 1));
    t.throws(() => divNI(NaN, 1));
    t.throws(() => divNI(new Date(), 1));
    t.throws(() => divNI(new RegExp(''), 1));
});


ava('modNI function', t => {
    t.is(modNI(), 0, 'no arg return 0');
    t.is(modNI(0), 0, 'zero arg return 0');
    t.is(modNI(1), 1, 'return its arg where arg is num and not zero');
    t.is(modNI(1, 2, 3, 4), 1, 'sequencial arg return ops of all element');
    t.is(modNI(1, -2, 3, -4), 1, 'sequencial arg return ops of all element');
    t.is(modNI(0, 1), 0);
    t.throws(() => modNI(Infinity));
    t.throws(() => modNI(1, 0));
    t.throws(() => modNI(-0, 0, 1, 2, 3, 4));
    t.throws(() => modNI(null));
    t.throws(() => modNI(undefined));
    t.throws(() => modNI(NaN));
    t.throws(() => modNI([]));
    t.throws(() => modNI({}));
    t.throws(() => modNI(new Date()));
    t.throws(() => modNI(new RegExp('')));
    t.throws(() => modNI(1, null));
    t.throws(() => modNI(1, undefined));
    t.throws(() => modNI(1, NaN));
    t.throws(() => modNI(1, new Date()));
    t.throws(() => modNI(1, new RegExp('')));
    t.throws(() => modNI(1, null, 2));
    t.throws(() => modNI(1, undefined, 2));
    t.throws(() => modNI(1, NaN, 2));
    t.throws(() => modNI(1, new Date(), 2));
    t.throws(() => modNI(1, new RegExp(''), 2));
    t.throws(() => modNI(1, null));
    t.throws(() => modNI(1, undefined));
    t.throws(() => modNI(1, NaN));
    t.throws(() => modNI(1, new Date()));
    t.throws(() => modNI(1, new RegExp('')));
    t.throws(() => modNI(null, 1));
    t.throws(() => modNI(undefined, 1));
    t.throws(() => modNI(NaN, 1));
    t.throws(() => modNI(new Date(), 1));
    t.throws(() => modNI(new RegExp(''), 1));
});

/* console.log('add', add());*/
/* console.log('add', add(1));*/
/* console.log('add', add(1, 2, 3, 4, 5));*/
/* console.log('sub', sub(1, 2, 3, 4, 5));*/
/* console.log('div', div(1, 2, 3, 4, 5));*/
/* console.log('add', add(1, 2, 3, 4, '5'));*/
/* console.log(add(1, 2, 3, 4, []));*/
/* console.log(add(1, 2, 3, 4, {}));*/
/* console.log(add(1, 2, 3, 4, Infinity));*/
/* console.log(addNI(1, 2, 3, 4, Infinity));*/
/* console.log(add(1, 2, 3, 4, NaN));*/
/* console.log(add(1, 2, 3, 4, true));*/
/* console.log(add(1, 2, 3, 4, false));*/
/* console.log(add(1, 2, 3, 4, ''));*/
/* console.log(add(1, 2, 3, 4, null));*/
/* console.log(add(1, 2, 3, 4, undefined));*/
