'use strict';

const ava = require('ava');
const fn = require('../src/functionals.js');

// let's define synchronous functions to use on test
const addition = (a, b) => a + b;
const addTwo = a => a + 2;
const powerOfTwo = a => Math.pow(a, 2);
const sum = collection => collection.reduce((init, next) => init + next);
const sumMultiArgs = (...args) => args.reduce((init, next) => init + next);


// let's define asynchronous functions to use on test
const longTask = param => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(param + 500), 100);
  });
};

const asyncSum = async(collection) => {
  return collection.reduce((init, next) => init + next);
};

ava('isFn function', t => {
  t.true(fn.isFn(() => {}), 'isFn on function');
  t.false(fn.isFn([1,2,3,4,5]), 'isFn on array');
  t.false(fn.isFn({a: 1, b: 2, c: 3}), 'isFn on object');
  t.false(fn.isFn(1), 'isFn on number');
  t.false(fn.isFn("foobar"), 'isFn on string');
  t.false(fn.isFn(null), 'isFn on null');
  t.false(fn.isFn(undefined), 'isFn on undefined');
  t.false(fn.isFn(), 'isFn on empty arg');
});


ava('isArr function', t => {
  t.true(fn.isArr([1,2,3,4,5]));
  t.false(fn.isArr({a: 1, b: 2, c: 3}));
  t.false(fn.isArr(1));
  t.false(fn.isArr("foobar"));
  t.false(fn.isArr(() => {}));
  t.false(fn.isArr(null));
  t.false(fn.isArr(undefined));
  t.false(fn.isArr());
});


ava('isObj function', t => {
  t.true(fn.isObj({a: 1, b: 2, c: 3}));
  t.false(fn.isObj([1,2,3,4,5]));
  t.false(fn.isObj(1));
  t.false(fn.isObj("foobar"));
  t.false(fn.isObj(() => {}));
  t.false(fn.isObj(null));
  t.false(fn.isObj(undefined));
  t.false(fn.isObj());
});

ava('count function', t => {
  t.is(fn.count([1,2,3,4,5]), 5, 'count work on array');
  t.is(fn.count({a: 1, b: 2, c: 3}), 3, 'count work on object');
  t.is(fn.count("string"), 6, 'count work on string');
  t.throws(() => fn.count(null), null, 'count throw error on null value');
  t.throws(() => fn.count(1), null, 'count throw error on number value');
  t.throws(() => fn.count(undefined), null, 'count throw error on undefined');
  t.throws(() => fn.count(), null, 'count on empty arg');
});

ava('isEmpty function', t => {
  t.true(fn.isEmpty([]), 'true on empty array');
  t.false(fn.isEmpty([1, 2, 3, 4]), 'false on array with element');
  t.true(fn.isEmpty({}), 'true on empty object');
  t.false(fn.isEmpty({a: 1}), 'false on object with element');
  t.true(fn.isEmpty(""), 'true on empty string');
  t.false(fn.isEmpty("foobar"), 'true on non empty string');
  t.throws(() => fn.isEmpty(1), null, 'error on number type value');
  t.throws(() => fn.isEmpty(null), null, 'error on null value');
  t.throws(() => fn.isEmpty(undefined), null, 'error on undefined');
  t.throws(() => fn.isEmpty(), null, 'isEmpty on empty arg');
});

ava('isStr function', t => {
  t.true(fn.isStr("foobar"), 'isStr work on string');
  t.false(fn.isStr([1, 2, 3, 4]), 'isStr work on array');
  t.false(fn.isStr({a: 1, b: 2}), 'isStr work on object');
  t.false(fn.isStr(null), 'isStr work on null');
  t.false(fn.isStr(1), 'isStr work on number');
  t.false(fn.isStr(undefined), 'isStr work on undefined');
  t.false(fn.isStr(true), 'isStr work on boolean true');
  t.false(fn.isStr(false), 'isStr on boolean false return null');
  t.false(fn.isStr(NaN), 'isStr work on NaN');
  t.false(fn.isStr(() => {}), 'isStr on function');
  t.false(fn.isStr(), 'isStr on empty arg');
});


ava('isColl function', t => {
  t.true(fn.isColl("foobar"), 'isColl work on string');
  t.true(fn.isColl([1, 2, 3, 4]), 'isColl work on array');
  t.true(fn.isColl({a: 1, b: 2}), 'isColl work on object');
  t.false(fn.isColl(null), 'isColl work on null');
  t.false(fn.isColl(1), 'isColl work on number');
  t.false(fn.isColl(undefined), 'isColl work on undefined');
  t.false(fn.isColl(true), 'isColl work on boolean true');
  t.false(fn.isColl(false), 'isColl on boolean false return null');
  t.false(fn.isColl(NaN), 'isColl work on NaN');
  t.false(fn.isColl(() => {}), 'isColl on function');
  t.false(fn.isColl(), 'isColl on empty arg');
});


ava('first function', t => {
  const a = [1, 2, 3, 4];
  const b = {a: 1, b: 2};
  fn.first(a);
  fn.first(b);
  t.deepEqual(a, [1, 2, 3, 4]);
  t.deepEqual(b, {a: 1, b: 2});

  t.is(fn.first([1, 2, 3, 4]), 1, 'first work on array');
  t.deepEqual(fn.first({a: 1, b: 2}), ["a", 1], 'first work on object');
  t.is(fn.first("foobar"), 'f', 'first work on string');
  t.is(fn.first([]), undefined, 'first on empty array return undefined');
  t.is(fn.first({}), undefined, 'first on empty object return undefined');
  t.is(fn.first(null), null, 'first on null return null');
  t.is(fn.first(1), null, 'first on number return null');
  t.is(fn.first(undefined), null, 'first on undefined return null');
  t.is(fn.first(true), null, 'first on boolean true return null');
  t.is(fn.first(false), null, 'first on boolean false return null');
  t.is(fn.first(NaN), null, 'first on NaN return null');
  t.is(fn.first(() => {}), null, 'first on function return null');
  t.is(fn.first(), null, 'first on empty arg');
});


ava('last function', t => {

  const a = [1, 2, 3, 4];
  const b = {a: 1, b: 2};
  fn.last(a);
  fn.last(b);
  t.deepEqual(a, [1, 2, 3, 4]);
  t.deepEqual(b, {a: 1, b: 2});

  t.is(fn.last([1, 2, 3, 4]), 4, 'last work on array');
  t.deepEqual(fn.last({a: 1, b: 2}), ["b", 2], 'last work on object');
  t.is(fn.last("foobar"), 'r', 'last work on string');
  t.is(fn.last([]), undefined, 'last on empty array return undefined');
  t.is(fn.last({}), undefined, 'last on empty object return undefined');
  t.is(fn.last(null), null, 'last on null return null');
  t.is(fn.last(1), null, 'last on number return null');
  t.is(fn.last(undefined), null, 'last on undefined return null');
  t.is(fn.last(true), null, 'last on boolean true return null');
  t.is(fn.last(false), null, 'last on boolean false return null');
  t.is(fn.last(NaN), null, 'last on NaN return null');
  t.is(fn.last(() => {}), null, 'last on function return null');
  t.is(fn.last(), null, 'last on empty arg');
});


ava('butLast function', t => {
  const a = [1, 2, 3, 4];
  const b = {a: 1, b: 2};
  fn.butLast(a);
  fn.butLast(b);
  t.deepEqual(a, [1, 2, 3, 4]);
  t.deepEqual(b, {a: 1, b: 2});


  t.deepEqual(fn.butLast([1, 2, 3, 4]), [1, 2, 3], 'butLast work on array');
  t.deepEqual(fn.butLast([1]), [1], 'butLast work on array');
  t.deepEqual(fn.butLast({a: 1, b: 2, c: 3}), [["a", 1], ["b", 2]], 'butLast work on object');
  t.deepEqual(fn.butLast({a: 1}), [["a", 1]], 'butLast work on object');
  t.is(fn.butLast("foobar"), 'fooba', 'butLast work on string');
  t.is(fn.butLast("f"), 'f', 'butLast work on string');
  t.is(fn.butLast(""), '', 'butLast work on string');
  t.deepEqual(fn.butLast([]), [], 'butLast on empty array return empty array');
  t.deepEqual(fn.butLast({}), [], 'butLast on empty object return empty object');
  t.is(fn.butLast(null), null, 'butLast on null return null');
  t.is(fn.butLast(1), null, 'butLast on number return null');
  t.is(fn.butLast(undefined), null, 'butLast on undefined return null');
  t.is(fn.butLast(true), null, 'butLast on boolean true return null');
  t.is(fn.butLast(false), null, 'butLast on boolean false return null');
  t.is(fn.butLast(NaN), null, 'butLast on NaN return null');
  t.is(fn.butLast(() => {}), null, 'butLast on function return null');
  t.is(fn.butLast(), null, 'butLast on empty arg');
});


ava('rest function', t => {
  const a = [1, 2, 3, 4];
  const b = {a: 1, b: 2};
  fn.rest(a);
  fn.rest(b);
  t.deepEqual(a, [1, 2, 3, 4]);
  t.deepEqual(b, {a: 1, b: 2});


  t.deepEqual(fn.rest([1, 2, 3, 4]), [2, 3, 4], 'rest work on array');
  t.deepEqual(fn.rest([1]), [1], 'rest on single item array');
  t.deepEqual(fn.rest({a: 1, b: 2, c: 3}), [["b", 2], ["c", 3]], 'rest work on object');
  /* t.deepEqual(fn.rest({a: 1}), [["a" 1]], 'rest on single item object');*/
  t.is(fn.rest("foobar"), 'oobar', 'rest work on string');
  t.is(fn.rest("f"), 'f', 'rest work on single char');
  t.is(fn.rest(""), '', 'rest work on empty string');
  t.deepEqual(fn.rest([]), [], 'rest on empty array return undefined');
  t.deepEqual(fn.rest({}), [], 'rest on empty object return undefined');
  t.is(fn.rest(null), null, 'rest on null return null');
  t.is(fn.rest(1), null, 'rest on number return null');
  t.is(fn.rest(undefined), null, 'rest on undefined return null');
  t.is(fn.rest(true), null, 'rest on boolean true return null');
  t.is(fn.rest(false), null, 'rest on boolean false return null');
  t.is(fn.rest(NaN), null, 'rest on NaN return null');
  t.is(fn.rest(() => {}), null, 'rest on function return null');
  t.is(fn.rest(), null, 'rest on empty arg');
});


ava('apply function', t => {
  t.is(fn.apply(sumMultiArgs, 1, 2, [3, 4]), 10, 'apply on 3 args and the last is coll');
  t.is(fn.apply(sumMultiArgs, 1, [2, 3, 4]), 10, 'apply on 2 args and the last is coll');
  t.is(fn.apply(sumMultiArgs, [1, 2, 3, 4]), 10, 'apply on single arg coll');
  t.throws(() => fn.apply(sumMultiArgs), null, 'apply on empty arg');
});


ava('synchronous partial function', t => {
  const partiallyAdded = fn.partial(addition, 2);
  t.is(partiallyAdded(10), 12);

  const partiallyAddTwo = fn.partial(addTwo);
  t.is(partiallyAddTwo(2), 4);

  const partiallySummed  = fn.partial(sum);
  t.is(partiallySummed([1, 2, 3, 4]), 10);
});


ava('asynchronous partial function', async t => {
  const _partiallyLongTask = fn.partial(longTask, 10);
  t.is(await _partiallyLongTask(), 510);

  const _partiallyAsyncSum = fn.partial(asyncSum);
  t.is(await _partiallyAsyncSum([1, 2, 3, 4]), 10);
});

ava('evaluate function', t => {
  t.is(fn.evaluate(`1 + 2`), 3, "a");
  t.is(fn.evaluate(`"hello"`), "hello", "b");
  t.is(fn.evaluate(fn.partial(powerOfTwo, 2)), 4, "c");
});

ava('doAll function', t => {
  const result = fn.doAll(`(1 + 1)`,
			  fn.partial(powerOfTwo, 2),
			  () => 30);
  t.is(result, 30);
});

ava('identity function', t => {
  t.is(fn.identity(10), 10);
  t.is(fn.identity(), undefined);
  t.deepEqual(fn.identity([1, 2, 3]), [1, 2, 3]);
});

ava('constantly function', t => {
  const a = {a: 1, b: 2};
  const ca = fn.constantly(a);
  a.a = 10;

  t.deepEqual(ca(), {a: 1, b: 2});

  const b = [1, 2, 3, 4, 5];
  const cb = fn.constantly(b);
  b.shift();

  t.deepEqual(cb(), [1, 2, 3, 4, 5]);

  const constantlyOne = fn.constantly(1);
  t.is(constantlyOne(60), 1);

  const constantlyCollections = fn.constantly([1, 2, 3]);
  t.deepEqual(constantlyCollections([19029283, 8989123]), [1, 2, 3]);
});

ava('compose function', t => {
  const composedFns1 = fn.compose(addition, addTwo, powerOfTwo);
  const composedFns2 = fn.compose(addition);
  t.is(composedFns1(1, 2), 25);
  t.is(composedFns2(1, 2), 3);
});


ava('asynchronous compose function', async t => {
  const addHundredAsync = async(arg) => await arg + 100;
  const _composedFns1A = fn._compose(longTask);
  const _composedFns1B = fn._compose(asyncSum);
  const _composedFns2A = fn._compose(asyncSum, longTask);
  const _composedFns2C = fn._compose(longTask, addHundredAsync);
  const _composedFns2B = fn._compose(asyncSum, addHundredAsync);
  const _composedFns3 = fn._compose(asyncSum, longTask, addHundredAsync);
  t.is(await _composedFns1A(100), 600);
  t.is(await _composedFns1B([100, 200, 300]), 600);
  t.is(await _composedFns2A([100, 200, 300]), 1100);
  t.is(await _composedFns2B([100, 200, 300]), 700);
  t.is(await _composedFns2C(100), 700);
  t.is(await _composedFns3([100, 200, 300]), 1200);

  const asyncFnError = async(param) => param.reduce(() => {});
  const _composedFns4 = fn._compose(asyncSum, asyncFnError);
  const _composedFns5 = fn._compose(asyncFnError, asyncSum);
  await t.throwsAsync(async () => _composedFns4([100, 200, 300]));
  await t.throwsAsync(async () => _composedFns5(1));
});


ava('map function', t => {

  const transducer = fn.map(i => ++i);

  t.deepEqual(fn.map(i => i, [1, 2, 3]), [1, 2, 3]);
  t.deepEqual(fn.map(i => i, {a: 1, b: 2, c: 3}), [["a", 1], ["b", 2], ["c", 3]]);
  t.deepEqual(fn.map(i => i, 'foobar'), ['f', 'o', 'o', 'b', 'a', 'r']);
  t.true(typeof (fn.map(i => i)) === 'function');
  t.deepEqual(transducer([1, 2, 3]), [2, 3, 4]);
  t.throws(() => fn.map(i => i, 1));
});

ava('reduce function', t => {
  const transducer = fn.reduce((init, next) => init + next);

  t.is(fn.reduce((init, next) => init + next, [1, 2, 3]), 6);
  t.deepEqual(fn.reduce((init, next) => [...init, next], [1, 2, 3], []), [1, 2, 3]);

  t.deepEqual(fn.reduce((init, next) => [...init, ...next], {a: 1, b: 2, c: 3}), ["a", 1, "b", 2, "c", 3]);
  t.deepEqual(fn.reduce((init, next) => [...init, ...next], {a: 1, b: 2, c: 3}, []), ["a", 1, "b", 2, "c", 3]);

  t.is(fn.reduce((init, next) => init + next, 'foobar'), 'foobar');
  t.is(fn.reduce((init, next) => init + next, 'foobar', null), 'foobar');
  t.deepEqual(fn.reduce((init, next) => [...init, next], 'foobar', []), ["f", "o", "o", "b", "a", "r"]);

  t.true(typeof (fn.reduce((init, next) => init + next)) === 'function');
  t.is(transducer([1, 2, 3]), 6);

});

ava('iterable function', t => {
  t.deepEqual(fn.iterable([1, 2, 3, 4]), [1, 2, 3, 4]);
  t.deepEqual(fn.iterable({a: 1, b: 2, c: 3}), [["a", 1], ["b", 2], ["c", 3]]);
  t.deepEqual(fn.iterable("foobar"), ["f", "o", "o", "b", "a", "r"]);
  t.deepEqual(fn.iterable([]), []);
  t.deepEqual(fn.iterable({}), []);
  t.deepEqual(fn.iterable(""), []);
  t.is(fn.iterable(1), null);
  t.is(fn.iterable(null), null);
  t.is(fn.iterable(undefined), null);
  t.is(fn.iterable(true), null);
  t.is(fn.iterable(false), null);
  t.is(fn.iterable(), null);
  t.is(fn.iterable(NaN), null);
  t.is(fn.iterable(Infinity), null);
});


ava('groupBy function', t => {
  /* a = [1, 2, 3, 1, 2, 4];
   * resA = fn.groupBy(fn.identity, a);
   * resA["1"] = null;
   * t.deepEqual(a, [1, 2, 3, 1, 2, 4]);*/

  /* b = fn.clone([{a: 1, b: 2}, {a: 1, b: 3}, {a: 2, b: 2}]);
   * resB = fn.groupBy(i => i.a, b);
   * resB["1"][0].b = 100;*/
  /* t.deepEqual(resB, {1: [{a: 1, b: 2}, {a: 1, b: 3}], 2: [{a: 2, b: 2}]});*/
  /* b = null;*/
  /* t.deepEqual(b, [{a: 1, b: 2}, {a: 1, b: 3}, {a: 2, b: 2}]);*/

  /* zz = 1000;
   * c = {a: 1, b: zz};
   * d = {a: 1, b: 3};
   * e = {a: 2, b: 2};
   * f = fn.clone([c, d, e]);
   * g = fn.clone(f);
   * resF = fn.groupBy(i => i.a, f);
   * c.a = 100;
   * zz = 2000;

   * t.deepEqual(resF, {1: [{a: 1, b: 1000}, {a: 1, b: 3}], 2: [{a: 2, b: 2}]});

   * resF["1"][1].b = 500;

   * t.deepEqual(f, [{a: 100, b: zz}, {a: 1, b: 3}, {a: 2, b: 2}]);

   * f[1].b = 1500;
   *
   * t.deepEqual(g, [{a: 1, b: 1000}, {a: 1, b: 3}, {a: 2, b: 2}]);*/


  t.deepEqual(fn.groupBy(fn.identity, [1, 2, 3, 1, 2, 4]), {1: [1,1], 2: [2, 2], 3: [3], 4: [4]});
  t.deepEqual(fn.groupBy(([key, val]) => val < 2, {a: 1, b: 2, c: 3}), {true: [["a", 1]], false: [["b", 2], ["c", 3]]});
  t.deepEqual(fn.groupBy(fn.identity, "foobar"), {"f": ["f"], "o": ["o", "o"], "b": ["b"], "a": ["a"], "r": ["r"] });
  t.deepEqual(fn.groupBy(fn.identity, []), {});
  t.deepEqual(fn.groupBy(fn.identity, {}), {});
  t.deepEqual(fn.groupBy(fn.identity, ""), {});
  t.is(fn.groupBy(fn.identity), null);
  t.is(fn.groupBy(fn.identity, null), null);
  t.is(fn.groupBy(fn.identity, undefined), null);
  t.is(fn.groupBy(fn.identity, true), null);
  t.is(fn.groupBy(fn.identity, false), null);
  t.is(fn.groupBy(fn.identity, NaN), null);
  t.is(fn.groupBy(fn.identity, Infinity), null);
  t.is(fn.groupBy(fn.identity, 1), null);
});


ava('freq function', t => {
  t.deepEqual(fn.freq([1, 2, 3, 1, 2, 3, 4]), {1: 2, 2: 2, 3: 2, 4: 1});
  t.deepEqual(fn.freq({a: 1, b: 2, c: 3}), {"a,1": 1, "b,2": 1, "c,3": 1});
  t.deepEqual(fn.freq([]), {});
  t.deepEqual(fn.freq({}), {});
  t.deepEqual(fn.freq(""), {});
  t.is(fn.freq(), null);
  t.is(fn.freq(null), null);
  t.is(fn.freq(undefined), null);
  t.is(fn.freq(NaN), null);
  t.is(fn.freq(1), null);
  t.is(fn.freq(Infinity), null);
  t.is(fn.freq(true), null);
  t.is(fn.freq(false), null);
});

ava('zipMap function', t => {
  t.deepEqual(fn.zipMap(["a", "b", "c", "d", "e"], [1, 2, 3, 4, 5, 6]), {"a": 1, "b": 2, "c": 3, "d": 4, "e": 5});
  t.deepEqual(fn.zipMap(["a", "b", "c", "d", "e"], [1, 2, 3]), {"a": 1, "b": 2, "c": 3});
  t.is(fn.zipMap(1, [1, 2, 3]), null);
});

ava('selectKeys function', t => {
  t.deepEqual(fn.selectKeys({a: 1, b: 2, c: 3}, ["c", "b"]), {c: 3, b: 2});
  t.is(fn.selectKeys(1, ["c", "b"]), null);
  t.deepEqual(fn.selectKeys({a: 1, b: 2, c: 3}, ["x", "y"]), {});
  t.is(fn.selectKeys({a: 1, b: 2, c: 3}, null), null);
});


ava('dropKeys function', t => {
  t.deepEqual(fn.dropKeys({a: 1, b: 2, c: 3}, ["c", "b"]), {a: 1});
  t.is(fn.dropKeys(1, ["c", "b"]), null);
  t.deepEqual(fn.dropKeys({a: 1, b: 2, c: 3}, ["x", "y"]), {a: 1, b: 2, c: 3});
  t.is(fn.dropKeys({a: 1, b: 2, c: 3}, null), null);
});


ava('get_ function', t => {
  t.is(fn.get_([0, 1, 2 ,3], 2), 2);
  t.is(fn.get_({a: 1, b: 2}, "a"), 1);
  t.is(fn.get_("foo", 0), null);
  t.is(fn.get_(1, 0), null);
  t.is(fn.get_(1, ""), null);
  t.is(fn.get_(), null);
  t.is(fn.get_(null, null), null);
});

ava('getIn function', t => {
  t.is(fn.getIn({a: 1 , b: 2, c: 3}, ["a"]), 1);
  t.is(fn.getIn({a: {a: 1} , b: 2, c: 3}, ["a", "a"]), 1);
  t.is(fn.getIn({a: [1, 2, {b: 3, c: [2]}]}, ["a", 2, "c", 0]), 2);
  t.is(fn.getIn([1, {a: [2, [2, 3 ,4, [10]]]}], [1, "a", 1, 3, 0]), 10);
  t.is(fn.getIn([1, {a: [2, [2, 3 ,4, [10]]]}], [1, "a", 0, 3, 0]), null);
  t.is(fn.getIn([], [1]), null);
  t.is(fn.getIn({}, [1]), null);
  t.is(fn.getIn({}, ["a"]), null);
  t.is(fn.getIn([], ["a"]), null);
  t.is(fn.getIn(1, [1]), null);
  t.is(fn.getIn("foobar", ["f"]), null);
  t.is(fn.getIn([1, 2, 3, 4], []), null);
  t.is(fn.getIn(null, []), null);
  t.is(fn.getIn(null, ["some"]), null);
  t.is(fn.getIn(null, [1]), null);
  t.is(fn.getIn({a: 1, b: 2}, [1]), undefined);
  t.is(fn.getIn(["res"], ["res"]), undefined);
  t.is(fn.getIn([1], ["res"]), undefined);
  t.is(fn.getIn(['a'], ['a']), undefined);
});

ava('clone function', t => {
  const zzz = {a: 1,b: [1, 2, 3, 4, [2, 3, 4, [5, 4, 6, 6 [1, 2, 3, [1, 2, 3, 4]]]]],c: [1, 2, 3, 4, {a: 1, b: [1, 2, 3, 4, {a: 1, b: 2, c: [{ a: 1, b: [{ a: 1, b: [{a: 1, b: [{a: 1,b: [1, 2, 3]},{a: 1,b: [1, 2, 3, {a: 1,b: [1, 2, 3, 4, [1, 3, 4, [5, 4, 3, [2, 1, 2, [1, 2, 3, 4, {a: 1,b: [1, 2, 3, { a: 1, b: [1, 2, 3, 4, [1, 3, 4, [5, 4, 3, [2, 1, 2, [1, 2, 3, 4, { a: 1, b: [1, ] }]]]]]}]}]]]]]}]}]}]}]}]}]}]};

  const a = {a: 1, b: {c: 3, d: {e: 4}}};
  const b = [1, 2, [3, 4, [5, 6]]];
  const c = {a: 1, b: [1, 2, 3, a], c: a};
  const d = [1, 2, 3, c];

  const ca = fn.clone(a);
  const cb = fn.clone(b);
  const cc = fn.clone(c);
  const cd = fn.clone(d);

  t.deepEqual(fn.clone(zzz), zzz);

  a.b.e = 10;
  t.deepEqual(ca, {a: 1, b: {c: 3, d: {e: 4}}});

  a.b = 1;
  t.deepEqual(ca, {a: 1, b: {c: 3, d: {e: 4}}});

  b.shift();
  t.deepEqual(cb, [1, 2, [3, 4, [5, 6]]]);

  b[1].shift();
  t.deepEqual(cb, [1, 2, [3, 4, [5, 6]]]);

  c.b.shift();
  t.deepEqual(cc, {a: 1, b: [1, 2, 3, {a: 1, b: {c: 3, d: {e: 4}}}], c: {a: 1, b: {c: 3, d: {e: 4}}}});

  d[3].b[2].a = 100;
  t.deepEqual(cd, [1, 2, 3, {a: 1, b: [1, 2, 3, {a: 1, b: {c: 3, d: {e: 4}}}], c: {a: 1, b: {c: 3, d: {e: 4}}}}]);

  t.deepEqual(ca, {a: 1, b: {c: 3, d: {e: 4}}});
  t.deepEqual(ca, {a: 1, b: {c: 3, d: {e: 4}}});
  t.deepEqual(cb, [1, 2, [3, 4, [5, 6]]]);
  t.deepEqual(cb, [1, 2, [3, 4, [5, 6]]]);
  t.is(fn.clone(1), 1);
  t.is(fn.clone('foobar'), 'foobar');
  t.is(fn.clone(), undefined);
});


ava('isGen function', t => {
  function* foo () { yield 1; }
  const bar = foo();

  t.true(fn.isGen(bar));
  t.false(fn.isGen([]));
  t.false(fn.isGen([1, 2, 3, 4]));
  t.false(fn.isGen({}));
  t.false(fn.isGen({a: 1}));
  t.false(fn.isGen(""));
  t.false(fn.isGen("foobar"));
  t.false(fn.isGen(1));
  t.false(fn.isGen(null));
  t.false(fn.isGen(undefined));
  t.false(fn.isGen());
});

ava('isNum function', t => {
  function* foo () { yield 1; }
  const bar = foo();

  t.false(fn.isNum(bar));
  t.false(fn.isNum([]));
  t.false(fn.isNum([1, 2, 3, 4]));
  t.false(fn.isNum({}));
  t.false(fn.isNum({a: 1}));
  t.false(fn.isNum(""));
  t.false(fn.isNum("foobar"));
  t.true(fn.isNum(1));
  t.false(fn.isNum(null));
  t.false(fn.isNum(undefined));
  t.false(fn.isNum());
});

ava('isOdd function', t => {
  function* foo () { yield 1; }
  const bar = foo();

  t.false(fn.isOdd(bar));
  t.false(fn.isOdd([]));
  t.false(fn.isOdd([1, 2, 3, 4]));
  t.false(fn.isOdd({}));
  t.false(fn.isOdd({a: 1}));
  t.false(fn.isOdd(""));
  t.false(fn.isOdd("foobar"));
  t.true(fn.isOdd(1));
  t.false(fn.isOdd(2));
  t.false(fn.isOdd(null));
  t.false(fn.isOdd(undefined));
  t.false(fn.isOdd());
});

ava('isEven function', t => {
  function* foo () { yield 1; }
  const bar = foo();

  t.false(fn.isEven(bar));
  t.false(fn.isEven([]));
  t.false(fn.isEven([1, 2, 3, 4]));
  t.false(fn.isEven({}));
  t.false(fn.isEven({a: 1}));
  t.false(fn.isEven(""));
  t.false(fn.isEven("foobar"));
  t.false(fn.isEven(1));
  t.true(fn.isEven(2));
  t.false(fn.isEven(null));
  t.false(fn.isEven(undefined));
  t.false(fn.isEven());
});

ava('reObj function', t => {
  t.deepEqual(fn.reObj([["a", 1], ["b", 2], ["c", 3], ["d", 4], ["e", 5]]), {a: 1, b: 2, c: 3, d: 4, e: 5});
  t.is(fn.reObj(), null);
  t.deepEqual(fn.reObj([]), {});
  t.is(fn.reObj({}), null);
  t.is(fn.reObj(1), null);
  t.is(fn.reObj("foobar"), null);
  t.throws(() => fn.reObj([1, 2, 3]));
});


ava('takeAll function', t => {
  function* foo() {
    yield 1;
    yield 2;
    yield 3;
  }
  const bar = foo();

  t.deepEqual(fn.takeAll(bar), [1, 2, 3]);
  t.deepEqual(fn.takeAll(fn.range(10)), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
  t.deepEqual(fn.takeAll([]), []);
  t.deepEqual(fn.takeAll({}), []);
  t.deepEqual(fn.takeAll(null), []);
  t.deepEqual(fn.takeAll(1), []);
  t.deepEqual(fn.takeAll("foobar"), []);
  t.deepEqual(fn.takeAll([1, 2, 3]), []);
  t.deepEqual(fn.takeAll({a: 1, b: 2}), []);
});


ava('range function', t => {
  t.true(typeof fn.range(10) === 'function');
  t.deepEqual(fn.takeAll(fn.range(-2, 2, -1)), [2, 1, 0, -1]);
  t.deepEqual(fn.takeAll(fn.range(10)), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]);
  t.deepEqual(fn.takeAll(fn.range(10, 2)), [2, 3, 4, 5, 6, 7, 8, 9]);
  t.deepEqual(fn.takeAll(fn.range(10, 2, 2)), [2, 4, 6, 8]);
  t.deepEqual(fn.takeAll(fn.range(10, 2, -1)), []);
  t.deepEqual(fn.takeAll(fn.range(0)), []);
  t.throws(() => fn.takeAll(fn.range()));
  t.throws(() => fn.takeAll(fn.range([])));
  t.throws(() => fn.takeAll(fn.range(1, {})));
  t.throws(() => fn.takeAll(fn.range(4, 2, {})));
});

ava('repeatedly function', t => {
  t.deepEqual(fn.takeAll(fn.repeatedly(5, () => 1)), [1, 1, 1, 1, 1]);
  t.true(typeof (fn.repeatedly(5, () => 1)) === 'function');
  t.throws(() => fn.repeatedly(5, null));
  t.throws(() => fn.repeatedly(null, () => {}));
});

ava('take function', t => {

  function* foo() {
    yield 1;
    yield 2;
    yield 3;
  }

  function* boo() {
    for (let i = 0; i < 1000; i++) {
      yield i;
    }
  }

  const bar = foo();
  const baz = boo();
  const foobar = foo();
  t.deepEqual(fn.take(4, foobar), [1, 2, 3]);
  t.deepEqual(fn.take(3, [1, 2, 3, 4, 5]), [1, 2, 3]);
  t.deepEqual(fn.take(3, fn.range(Infinity)), [0, 1, 2]);
  t.deepEqual(fn.take(3, fn.range(2)), [0, 1]);
  t.deepEqual(fn.take(2, bar), [1, 2]);
  t.deepEqual(fn.take(3, baz), [0, 1, 2]);
  t.deepEqual(fn.take(0, [0, 1, 2]), []);
  t.deepEqual(fn.take(1, [0, 1, 2]), [0]);
  t.deepEqual(fn.take(0, "foobar"), []);
  t.deepEqual(fn.take(0, 1), []);
  t.throws(() => fn.take(null, [1, 2, 3]));
});

ava('drop function', t => {

  function* foo() {
    yield 1;
    yield 2;
    yield 3;
  }

  const bar = foo();


  t.deepEqual(fn.drop(3, [1, 2, 3, 4, 5]), [4, 5]);
  t.deepEqual(fn.drop(3, fn.range(5)), [3, 4]);
  t.deepEqual(fn.drop(0, fn.range(5)), [0, 1, 2, 3, 4]);
  t.deepEqual(fn.drop(2, bar), [3]);
  t.deepEqual(fn.drop(0, [1, 2, 3, 4, 5]), [1, 2, 3, 4, 5]);
  t.deepEqual(fn.drop(1, [1, 2, 3, 4, 5]), [2, 3, 4, 5]);
  t.deepEqual(fn.drop(0, 1), []);
  t.throws(() => fn.drop(null, [1, 2, 3]));
});

ava('takeNth function', t => {
  t.deepEqual(fn.takeNth(2, [1, 2, 3, 4, 5]), [2, 4]);
  t.deepEqual(fn.takeNth(3, fn.takeAll(fn.range(10))), [2, 5, 8]);
  t.deepEqual(fn.takeNth(1, [1, 2, 3, 4, 5]), [1, 2, 3, 4, 5]);
  t.deepEqual(fn.takeNth(0, [1, 2, 3, 4, 5]), []);
  t.throws(() => fn.takeNth(null, [1, 2, 3]));
});

ava('dropNth function', t => {
  t.deepEqual(fn.dropNth(2, [1, 2, 3, 4, 5]), [1, 3, 5]);
  t.deepEqual(fn.dropNth(3, fn.takeAll(fn.range(10))), [0, 1, 3, 4, 6, 7, 9]);
  t.deepEqual(fn.dropNth(1, [1, 2, 3, 4, 5]), []);
  t.deepEqual(fn.dropNth(0, [1, 2, 3, 4, 5]), [1, 2, 3, 4, 5]);
  t.throws(() => fn.dropNth(null, [1, 2, 3]));
  t.throws(() => fn.dropNth([], [1, 2, 3]));
});


ava('trampoline function', t => {
  const fnCurry = () => () => 1;
  t.is(fn.trampoline(fnCurry), 1);
  t.is(fn.trampoline(() => 1), 1);
  t.is(fn.trampoline(() => () => () => () => () => () => 1), 1);
});


ava('assoc function', t => {
  t.deepEqual(fn.assoc([], 0, 1, 1, 3, 2, 5), [1, 3, 5]);
  t.deepEqual(fn.assoc({a: 1}, "b", 2, "c", 3), {a: 1, b: 2, c: 3});
  t.deepEqual(fn.apply(fn.assoc, null, fn.takeAll(fn.range(10))), { '0': 1, '2': 3, '4': 5, '6': 7, '8': 9 });
  t.throws(() => fn.assoc(null, 1, 2, 3));
  t.throws(() => fn.assoc([], 1, 2, 3));
});


ava('assocIn function', t => {
  t.deepEqual(fn.assocIn({a: 1, b: [{a: 1, b: 100}]}, ["b", 0, "b"], 200), {a: 1, b: [{a: 1, b: 200}]});
  t.deepEqual(fn.assocIn({}, [1], 100), {'1': 100});
  t.deepEqual(fn.assocIn({a: 1, b: []}, ["b", 0], 100), {a: 1, b: [100]});
  t.deepEqual(fn.assocIn([{a: 1, b: 2}, {a: 1, b: 2}], [1, "b"], 100), [{a: 1, b: 2}, {a: 1, b: 100}]);
});

ava('condNext function', t => {
  t.is(fn.condNext(1,
		   [true, arg => arg + 1],
		   [1,  arg => arg + 10],
		   [() => false,  arg => arg * 100],
		   [() => true,  arg => arg * 200]
		  ), 2400);

  t.is(fn.condNext(2,
		   [() => 1 < 2, arg => arg + 1],
		   [true && true,  arg => arg + 10],
		   [1,  arg => arg * 100]
		  ), 1300);
});


ava('promiseable function', async t => {
  const withCallback1Arg = (arg, callback) => {
    setTimeout(() => callback(arg), 300);
  };

  const withCallbackMoreArg = (arg1, arg2,  callback) => {
    setTimeout(() => callback(arg1, arg2), 300);
  };

  const withCallbackNullErrorMoreArg = (arg1, arg2,  callback) => {
    setTimeout(() => callback(null, arg1, arg2), 300);
  };

  const withCallbackError = (arg, callback) => {
    setTimeout(() => callback(new Error(), arg), 300);
  };

  const withCallbackErrorAndMore = (arg1, arg2, callback) => {
    setTimeout(() => callback(new Error(), arg1, arg2), 300);
  };

  const withPromise1Arg = fn.promiseable(withCallback1Arg);
  const withPromiseMoreArg = fn.promiseable(withCallbackMoreArg);
  const withPromiseNullErrorMoreArg = fn.promiseable(withCallbackNullErrorMoreArg);
  const withPromiseError = fn.promiseable(withCallbackError);
  const withPromiseErrorAndMore = fn.promiseable(withCallbackErrorAndMore);

  t.is(await withPromise1Arg(1), 1);
  t.deepEqual(await withPromiseMoreArg(1, 2), [1, 2]);
  t.deepEqual(await withPromiseNullErrorMoreArg(1, 2), [1, 2]);
  await t.throwsAsync(() => withPromiseError(1));
  await t.throwsAsync(() => withPromiseErrorAndMore(1, 2));
});

ava('execMethod function', t => {
  const someObject = {
    value: 1,
    get: function(){
      return this.value;
    },
    inc: function(){
      return this.value =+ 1;

    },
    add: function(arg){
      return this.value =+ arg;
    }
  };

  t.is(fn.execMethod(someObject, "get")(), 1);
  t.is(fn.execMethod(someObject, "inc")(), 1);
  t.is(fn.execMethod(someObject, "add")(2), 2);
  t.is(fn.execMethod(someObject, "get")(), 2);
});


ava('str function', t => {
  t.is(fn.str(1, 2, 3, 4, 5, 6, 7), "1234567");
  t.is(fn.str(1, "a", "hello", [1, 2], {a: 1, b: 2}), "1ahello1,2[object Object]");
  t.is(fn.str(), "");
  t.is(fn.str(null, undefined, NaN), "NaN");
});


ava('flatten function', t => {
  t.deepEqual(fn.flatten([1, 2, 3]), [1, 2, 3]);
  t.deepEqual(fn.flatten([]), []);
  t.deepEqual(fn.flatten(), []);
  t.deepEqual(fn.flatten([[1,2,3], [1, 2, 3, [0, [1, {a: 1, b: 2, c: [1, 2]}]]], [1, 2, [2, 3, 4]]]),
	      [1, 2, 3, 1, 2, 3, 0, 1, {a: 1, b: 2, c: [1, 2]}, 1, 2, 2, 3, 4]);
  t.deepEqual(fn.flatten([[1,2,3, {a: 1}, [1, 10]], [1, 2, 3, [0, [1, {a: 1, b: 2, c: [1, 2]}]]], [1, 2, [2, 3, 4]]]),
	      [1, 2, 3, {a: 1}, 1, 10, 1, 2, 3, 0, 1, {a: 1, b: 2, c: [1, 2]}, 1, 2, 2, 3, 4]);
});

ava('merge function', t => {
  t.deepEqual(fn.merge({a: 1, b: 2}, {a: 2, c: 4}), {a: 2, b: 2, c: 4});
  t.deepEqual(fn.merge({a: 1, b: 2}, {a: 2, c: 4}, {x: 2, y: 2, z: 0}), {a: 2, b: 2, c: 4, x: 2, y: 2, z: 0});
  t.deepEqual(fn.merge({a: 1, b: 2}, [1, 2, 3, 4]), { '0': 1, '1': 2, '2': 3, '3': 4, a: 1, b: 2 });
  // t.deepEqual(fn.merge({a: 1, b: 2}, 1), { a: 1, b: 2, null: 1 });
  t.deepEqual(fn.merge(null, {a: 2, c: 4}), {a: 2, c: 4});
  t.deepEqual(fn.merge({a: 1, b: 2}), { a: 1, b: 2 });
  t.deepEqual(fn.merge([1, 2, 3], {a: 2, c: 4}), [ 1, 2, 3, [ 'a', 2 ], [ 'c', 4 ] ]);
  t.deepEqual(fn.merge([1, 2, 3], [1, 2, 3, 4]), [1, 2, 3, 1, 2, 3, 4]);
  t.deepEqual(fn.merge([1, 2, 3], [1, 2, 3, 4], [20, 21, 22, 23]), [1, 2, 3, 1, 2, 3, 4, 20, 21, 22, 23]);
  t.deepEqual(fn.merge([1, 2, 3], 1), [ 1, 2, 3, 1 ]);
  t.deepEqual(fn.merge([1, 2, 3]), [ 1, 2, 3 ]);
  t.deepEqual(fn.merge({a: 2, c: 4}, null), {a: 2, c: 4});
  t.throws(() => fn.merge({a: 1, b: 2}, 1));
  // t.throws(() => fn.merge(null, {a: 2, c: 4}));
  t.throws(() => fn.merge(1, [1, 2, 3, 4]));
  t.throws(() => fn.merge("foobar", 1));
  t.throws(() => fn.merge(new Date(), {a: 2, c: 4}));
});

ava('mergeWith function', t => {
  t.deepEqual(fn.mergeWith((a, b) => b, {a: 1, b: 2}, {a: 2, c: 4}), {a: 2, b: 2, c: 4});
  t.deepEqual(fn.mergeWith((a, b) => b, {a: 1, b: 2}, {a: 2, c: 4}, {x: 2, y: 2, z: 0}), {a: 2, b: 2, c: 4, x: 2, y: 2, z: 0});
  t.deepEqual(fn.mergeWith((a, b) => b, {a: 1, b: 2}, [1, 2, 3, 4]), { '0': 1, '1': 2, '2': 3, '3': 4, a: 1, b: 2 });
  t.deepEqual(fn.mergeWith((a, b) => b, null, {a: 2, c: 4}), {a: 2, c: 4});
  t.deepEqual(fn.mergeWith((a, b) => b, {a: 1, b: 2}), { a: 1, b: 2 });
  t.deepEqual(fn.mergeWith((a, b) => b, [1, 2, 3], {a: 2, c: 4}), [ 1, 2, 3, [ 'a', 2 ], [ 'c', 4 ] ]);
  t.deepEqual(fn.mergeWith((a, b) => b, [1, 2, 3], [1, 2, 3, 4]), [1, 2, 3, 1, 2, 3, 4]);
  t.deepEqual(fn.mergeWith((a, b) => b, [1, 2, 3], [1, 2, 3, 4], [20, 21, 22, 23]), [1, 2, 3, 1, 2, 3, 4, 20, 21, 22, 23]);
  t.deepEqual(fn.mergeWith((a, b) => b, [1, 2, 3], 1), [ 1, 2, 3, 1 ]);
  t.deepEqual(fn.mergeWith((a, b) => b, [1, 2, 3]), [ 1, 2, 3 ]);
  t.deepEqual(fn.mergeWith((a, b) => b, {a: 2, c: 4}, null), {a: 2, c: 4});
  t.throws(() => fn.mergeWith((a, b) => b, {a: 1, b: 2}, {a: 2, c: 4}, 1));
  t.throws(() => fn.mergeWith(null, {a: 1, b: 2}, 1));
  t.throws(() => fn.mergeWith((a, b) => b, 1, [1, 2, 3, 4]));
  t.throws(() => fn.mergeWith("foobar", 1));
  t.throws(() => fn.mergeWith(new Date(), {a: 2, c: 4}));
});

ava('deepMerge function', t => {
  t.deepEqual(
    {
      a: 1,
      x: 100,
      b: {c: {d: {e: [1, 2, 3, 4, 5, 6]}}, f: 50},
      c: [1, [2, [3, [4, {x: 1, y: 2, z: [1, 2, 3]}]]], 1, [2, [3, [4, {z: [4, 5 ,6]}]]]],
      d: []
    },
    fn.deepMerge(
      {
	a: 1,
	b: {c: {d: {e: [1, 2, 3]}}},
	c: [1, [2, [3, [4, {x: 1, y: 2, z: [1, 2, 3]}]]]],
	d: 100
      },
      {
	a: 1,
	x: 100,
	b: {c: {d: {e: [4, 5, 6]}}, f: 50},
	c: [1, [2, [3, [4, {z: [4, 5 ,6]}]]]],
	d: []
      }
    ),
  );

  t.deepEqual(
    {
      a: 1,
      x: 100,
      b: {c: {d: {e: [1, 2, 3, 4, 5, 6]}}, f: 50},
      c: [1, [2, [3, [4, {x: 1, y: 2, z: [1, 2, 3]}]]], 1, [2, [3, [4, {z: [4, 5 ,6]}]]]],
      d: [1, 2, 3],
      z: 100,
    },
    fn.deepMerge(
      {
	a: 1,
	b: {c: {d: {e: [1, 2, 3]}}},
	c: [1, [2, [3, [4, {x: 1, y: 2, z: [1, 2, 3]}]]]],
	d: 100
      },
      {
	a: 1,
	x: 100,
	b: {c: {d: {e: [4, 5, 6]}}, f: 50},
	c: [1, [2, [3, [4, {z: [4, 5 ,6]}]]]],
	d: []
      },
      {
	z: 100,
	d: [1, 2, 3],
	x: null
      }
    ),
  );
});


ava('updateIn function', t => {
  t.deepEqual(fn.updateIn({a: 1, b: 2, c: 3}, ["a"], (o, n) => o + n, 10), {a: 11, b: 2, c: 3});
  t.deepEqual(fn.updateIn({a: 1, b: 2, c: 3}, ["a"], (o, n) => o + n, 10), {a: 11, b: 2, c: 3});
  t.deepEqual(fn.updateIn({a: 1, b: [{c: 3}]}, ["b", 0], fn.merge, {a: 10}), {a: 1, b: [{c: 3, a: 10}]});
  t.throws(() => fn.updateIn({a: 1, b: 2}, ["a"], null, null));
});


ava('isEqual function', t => {
  t.true(fn.isEqual(1, 1));
  t.true(fn.isEqual(null, null));
  t.true(fn.isEqual());
  t.true(fn.isEqual([1], [1]));
  t.true(fn.isEqual([1, 2], [1, 2]));
  t.true(fn.isEqual({a: 1}, {a: 1}));
  t.true(fn.isEqual({a: 1, b: 2}, {a: 1, b: 2}));
  t.true(fn.isEqual([1, [2, [3, 4, [5, 6, [7, 8], [10]], 1, 2]]], [1, [2, [3, 4, [5, 6, [7, 8], [10]], 1, 2]]]));
  t.true(fn.isEqual({a: 1, b: 2, c: {d: {e: [1, 2, 3, [2, 3, 4, [3, {1: 1, 2: 2, 3: [1, 2, {a: 1}]}]]]}}},
		    {a: 1, b: 2, c: {d: {e: [1, 2, 3, [2, 3, 4, [3, {1: 1, 2: 2, 3: [1, 2, {a: 1}]}]]]}}}));
  t.false(fn.isEqual(1, 2));
  t.false(fn.isEqual(null, undefined));
  t.false(fn.isEqual(0, false));
  t.false(fn.isEqual([], [1]));
  t.false(fn.isEqual({a: 1}, {a: 2}));
  t.false(fn.isEqual({b: 1}, {a: 1}));
  t.false(fn.isEqual({b: 1}, {c: 1}));
  t.false(fn.isEqual([null, undefined, NaN, Infinity], [undefined, null, NaN, Infinity]));
  t.false(fn.isEqual([1, {a: 1}], [1, null]));
  t.false(fn.isEqual([1, {a: 1}], [1, [1]]));
  t.false(fn.isEqual({a: 1, b: {c: 3}}, {a: 1, b: [1, 2]}));
  t.false(fn.isEqual({a: 1, b: [1, 2]}, {a: 1, b: {c: 3}}));
  t.false(fn.isEqual({a: 1, b: 2}, {a: 1, b: {c: 3}}));
  t.false(fn.isEqual({a: 1, b: 2, c: {d: {e: [1, 2, 3, [2, 3, 4, [3, {1: 1, 2: 2, 3: [1, 2, {a: 1}]}]]]}}},
		     {a: 1, b: 2, c: {d: {e: [1, 2, 3, [2, 3, 4, [3, {1: 1, 2: 4, 3: [1, 2, {a: 1}]}]]]}}}));
  t.false(fn.isEqual({a: 1, b: 2, c: {d: {e: [1, 2, 3, [2, 3, 4, [3, {1: 1, 2: 2, 3: [1, 2, {a: 1}]}]]]}}},
		     {a: 1, b: 2, c: {d: {e: [1, 2, 3, [2, 4, 4, [3, {1: 1, 2: 2, 3: [1, 2, {a: 1}]}]]]}}}));
  t.false(fn.isEqual({a: 1, b: 2, c: {d: {e: [1, 2, 3, [2, 3, 4, [3, {1: 1, 2: 2, 3: [1, 2, {a: 1}]}]]]}}},
		     {a: 1, b: 2, c: {d: {e: [1, 2, 3, [2, 3, 4, [3, {1: 2, 2: 2, 3: [1, 2, {a: 1}]}]]]}}}));
  t.false(fn.isEqual(["a", 1], {a: 1}));
  let d1, d2 = new Date()
  t.false(fn.isEqual(d1, d2));
  let r1, r2 = new RegExp('')
  t.false(fn.isEqual(r1, r2));
  let r3, r4 = new RegExp('acb+')
  t.false(fn.isEqual(r3, r4));
  t.true(fn.isEqual(new RegExp('abc+'), new RegExp('abc+')));
});


ava('pos functions', t => {
  const arr = [1, null, undefined, {a: 1}, [], [1], [1, {a: 1}], [1, null]];
  t.deepEqual(fn.pos(arr, 1), [0, 1]);
  t.deepEqual(fn.pos(arr, [1, null]), [7, [1, null]]);
  t.deepEqual(fn.pos(arr, [1, {a: 1}]), [6, [1, {a: 1}]]);
  t.deepEqual(fn.pos(arr), [2, undefined]);
  t.is(fn.pos(arr, false), null);
  t.throws(() => fn.pos());
  t.throws(() => fn.pos(1, 1));
  t.throws(() => fn.pos({a: 1}, 2));
});


ava('dedupe function', t => {
  t.deepEqual(fn.dedupe([1, 1, 2, 3, 5, 5, 6, 7]), [1, 2, 3, 5, 6, 7]);
  t.deepEqual(fn.dedupe([1, 1, 2, 5, 5, {a: 1, b: 2}, {a: 1, b: 2}, {a: 1}]), [1, 2, 5, {a: 1, b: 2}, {a: 1}]);
  t.deepEqual(fn.dedupe({a: 1}), {a: 1});
  t.is(fn.dedupe(1), 1);
  t.deepEqual(fn.dedupe([[1], [1], [2, 3], [1, 2], [1]]), [[1], [2, 3], [1, 2]]);
  t.deepEqual(fn.dedupe([{a: 1, b: 2}, {a: 1, b: 2}, {a: 1, b: 2}, {c: 3, d: 4}, {a: 2, b: 2}]),
	      [{a: 1, b: 2}, {c: 3, d: 4}, {a: 2, b: 2}]);
});
